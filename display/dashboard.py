import Tkinter as tk
import tkFont as tkfont
from PIL import Image, ImageTk
import datetime
import ttk

def_w = 1366
h_comp = 65
batas_lane = 8
image_display_path = None
title_official = None
image_label = None


class App(tk.Tk):

    def __init__(self,*args,**kwargs):
        tk.Tk.__init__(self,*args,**kwargs)
        self.title("DisMan-Proto")
        self.geometry("1366x768")

        self.title_font = tkfont.Font(family='Verdana', size=18, weight="bold", slant="italic")

        container = tk.Frame(self)
        container.pack(side="top",fill="both",expand=True)
        container.grid_rowconfigure(0,weight=1)
        container.grid_columnconfigure(0,weight=1)

        self.frames = {}
        self.frames["Display"] = Display(parent=container, controller=self)
        self.frames["FinalDisplay"] = FinalAnnounceDisplay(parent=container, controller=self)
        self.frames["MedalDisplay"] = MedalDisplay(parent=container, controller=self)
        self.frames["ImageDisplay"] = ImageDisplay(parent=container, controller=self)
        self.frames["AnnounceDisplay"] = AnnounceDisplay(parent=container, controller=self)

        self.frames["Display"].grid(row=0, column=0, sticky="nsew")
        self.frames["FinalDisplay"].grid(row=0, column=0, sticky="nsew")
        self.frames["MedalDisplay"].grid(row=0, column=0, sticky="nsew")
        self.frames["ImageDisplay"].grid(row=0, column=0, sticky="nsew")
        self.frames["AnnounceDisplay"].grid(row=0, column=0, sticky="nsew")

        self.show_frame("Display")

    def show_frame(self,page_name):
        frame = self.frames[page_name]
        frame.tkraise()

class Display(tk.Frame):

    def __init__(self,parent,controller):
        global def_w,h_comp,batas_lane
        global title_official

        tk.Frame.__init__(self,parent)
        self.controller = controller

        # Frame Title
        frame_title=tk.Frame(self,width=def_w,height=90,bg="black")
        frame_title.pack_propagate(0)
        frame_title.pack()

        frame_official=tk.Frame(self,width=def_w,height=40,bg="gray")
        frame_official.pack_propagate(0)
        frame_official.pack()

        # Frame Competitor
        competitor_frames=[]
        for i in range(0,batas_lane):
            temp = tk.Frame(self,width=def_w,height=h_comp,bg="gray",bd=1)
            competitor_frames.append(temp)
        for i in range(0,batas_lane):
            competitor_frames[i].pack()
            separator = tk.Frame(self,height=2,bd=1,relief=tk.SUNKEN)
            separator.pack()

        #Frame Timer
        frame_timer=tk.Frame(self,width=def_w,height=102,bg="white")
        frame_timer.pack_propagate(0)
        frame_timer.pack()

        ###Competitor Content Frames - Start
        lane_frames=[]
        # Frame Lane - Start
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            lane_frames.append(temp)
        for i in range(0, batas_lane):
            lane_frames[i].pack_propagate(0)
            lane_frames[i].pack(side=tk.LEFT)
            separator1 = tk.Frame(width=2, height=65, bd=1, relief=tk.SUNKEN)
            separator1.pack(side=tk.LEFT)
        # Frame Lane - Stop

        # Frame Team - Start
        team_frames=[]
        logo_frames=[]
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
            team_frames.append(temp)
            logo_frames.append(temp1)
        for i in range(0, batas_lane):
            team_frames[i].pack_propagate(0)
            team_frames[i].pack(side=tk.LEFT)
            logo_frames[i].pack_propagate(0)
            logo_frames[i].pack(side=tk.LEFT)
        # Frame Team - Stop

        # Frame CompName - Start
        name_frames=[]
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=600, height=h_comp, bg="pink4")
            name_frames.append(temp1)
        for i in range(0, batas_lane):
            name_frames[i].pack_propagate(0)
            name_frames[i].pack(side=tk.LEFT)
        # Frame CompName - Stop

        # Frame Pos - Start
        pos_frames=[]
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            pos_frames.append(temp1)
        for i in range(0, batas_lane):
            pos_frames[i].pack_propagate(0)
            pos_frames[i].pack(side=tk.LEFT)
            separator2 = tk.Frame(competitor_frames[i], width=2, height=h_comp, bd=1, relief=tk.SUNKEN, bg="black")
            separator2.pack(side=tk.LEFT)
        # Frame Pos - Stop

        # Frame Status - Start
        status_frames=[]
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=364, height=h_comp, bg="white")
            status_frames.append(temp1)
        for i in range(0, batas_lane):
            status_frames[i].pack_propagate(0)
            status_frames[i].pack(side=tk.LEFT)
        # Frame Status - Stop
        ###Competitor Content Frames - Stop

        ###Title Content
        image = Image.open("../imgs/logosss.png")
        photo = ImageTk.PhotoImage(image)
        logo_label = tk.Label(frame_title, image=photo)
        logo_label.image = photo
        logo_label.pack(side="left")

        title_label = tk.Label(frame_title, text="<Event>", bg="black", fg="white", font=("Verdana", 30, "bold"))
        title_label.pack()

        mode_label = tk.Label(frame_title, text="", bg="black", fg="white", font=("Verdana", 30, "bold"))
        mode_label.pack()

        title_official = tk.Label(frame_official,text="",bg="gray",fg="black",font=("Verdana",15,"bold"))
        title_official.pack()
        ###Title Content

        ## Timer
        sep = tk.Frame(frame_timer, width=20, height=65, relief=tk.SUNKEN, bg="white")
        sep.pack(side=tk.RIGHT)
        waktu_skrg = str(datetime.datetime.time(datetime.datetime.now()))
        try:
            waktu_skrg, sisa = waktu_skrg.split('.')
            print waktu_skrg
        except:
            print "masuk except"
            print waktu_skrg
        timer_now = tk.Label(frame_timer, text=waktu_skrg, fg="black", bg="white", font=("Verdana", 35, "bold"))
        timer_now.pack(side=tk.LEFT)

        timer_label = tk.Label(frame_timer, text=0.0, fg="black", bg="white", font=("Verdana", 40, "bold"))
        timer_label.pack(side=tk.RIGHT)
        ## Timer - Stop

        ###Lane Numbers - Start
        lane_labels=[]
        for i in range(0, batas_lane):
            lane = tk.Label(lane_frames[i], text="L" + str(i + 1), bg="white", fg="black", font=("Verdana", 40, "bold"))
            lane_labels.append(lane)
        for element in lane_labels:
            element.pack()
        ###Lane Numbers - Stop

        # th = Thread(target=lambda: listener(home, timer_label, status_frames, pos_frames, name_frames))
        # th.start()

        #####
        controlPanel = tk.Toplevel()
        controlPanel.title("Control Panel")

        rows=0
        while rows<30:
            controlPanel.rowconfigure(rows,weight=1)
            controlPanel.columnconfigure(rows,weight=1)
            rows += 1

        nb = ttk.Notebook(controlPanel)
        nb.grid(row=0,column=0,columnspan=30,rowspan=30,sticky='nesw')

        event_control = ttk.Frame(nb)
        nb.add(event_control, text='Event')

        final_announcement_control = ttk.Frame(nb)
        nb.add(final_announcement_control, text='Final Event Announcement')

        medal_control = ttk.Frame(nb)
        nb.add(medal_control, text='Medals')

        image_control = ttk.Frame(nb)
        nb.add(image_control, text='Graphics')

        announcement_control = ttk.Frame(nb)
        nb.add(announcement_control, text='Announcement')
        #####

        # Event Control Panel
        var = tk.StringVar(event_control)
        var.set("Choose One")  # initial value
        # options = get_events()
        # option = tk.apply(OptionMenu, (controlPanel, var) + tuple(options))
        # option.grid(row=0, column=0)

        buttonEvent = tk.Button(event_control, text="Choose Events", command=lambda: getText(var, title_label, mode_label))
        buttonEvent.grid(row=0, column=1)

        var1 = tk.StringVar(event_control)
        var1.set("Choose One")  # initial value
        # options1 = [1, 2, 3, 4, 5]
        # option1 = tk.apply(OptionMenu, (controlPanel, var1) + tuple(options1))
        # option1.grid(row=1, column=0)

        butHeat = tk.Button(event_control, text="Choose Heat", command=lambda: getHeat(var1, mode_label,[team_frames, logo_frames, name_frames, pos_frames, status_frames]))
        butHeat.grid(row=1, column=1)
        butPes = tk.Button(event_control, text="Show",command=lambda: get_pesertas(queryyy[0], queryyy[2], heatt, name_frames, team_frames))
        butPes.grid(row=2)

        # buttons
        teks = []
        for i in range(0, batas_lane):
            teks.append(i)
        butDQ = tk.Button(event_control, text="L" + str(teks[0] + 1) + "-DQ", command=lambda: disq(status_frames, teks[0]))
        butDQ.grid(row=3, column=3)

        butDQ1 = tk.Button(event_control, text="L" + str(teks[1] + 1) + "-DQ", command=lambda: disq(status_frames, teks[1]))
        butDQ1.grid(row=3, column=4)

        butDQ2 = tk.Button(event_control, text="L" + str(teks[2] + 1) + "-DQ", command=lambda: disq(status_frames, teks[2]))
        butDQ2.grid(row=3, column=5)

        butDQ3 = tk.Button(event_control, text="L" + str(teks[3] + 1) + "-DQ", command=lambda: disq(status_frames, teks[3]))
        butDQ3.grid(row=3, column=6)

        butDQ4 = tk.Button(event_control, text="L" + str(teks[4] + 1) + "-DQ", command=lambda: disq(status_frames, teks[4]))
        butDQ4.grid(row=3, column=7)

        butDQ5 = tk.Button(event_control, text="L" + str(teks[5] + 1) + "-DQ", command=lambda: disq(status_frames, teks[5]))
        butDQ5.grid(row=3, column=8)

        butDQ6 = tk.Button(event_control, text="L" + str(teks[6] + 1) + "-DQ", command=lambda: disq(status_frames, teks[6]))
        butDQ6.grid(row=3, column=9)

        butDQ7 = tk.Button(event_control, text="L" + str(teks[7] + 1) + "-DQ", command=lambda: disq(status_frames, teks[7]))
        butDQ7.grid(row=3, column=10)
        # buttons

        butDNS = tk.Button(event_control, text="L" + str(teks[0] + 1) + "-DNS", command=lambda: dns(status_frames, teks[0]))
        butDNS.grid(row=4, column=3)

        butDNS1 = tk.Button(event_control, text="L" + str(teks[1] + 1) + "-DNS",command=lambda: dns(status_frames, teks[1]))
        butDNS1.grid(row=4, column=4)

        butDNS2 = tk.Button(event_control, text="L" + str(teks[2] + 1) + "-DNS",command=lambda: dns(status_frames, teks[2]))
        butDNS2.grid(row=4, column=5)

        butDNS3 = tk.Button(event_control, text="L" + str(teks[3] + 1) + "-DNS",
                         command=lambda: dns(status_frames, teks[3]))
        butDNS3.grid(row=4, column=6)

        butDNS4 = tk.Button(event_control, text="L" + str(teks[4] + 1) + "-DNS",
                         command=lambda: dns(status_frames, teks[4]))
        butDNS4.grid(row=4, column=7)

        butDNS5 = tk.Button(event_control, text="L" + str(teks[5] + 1) + "-DNS",
                         command=lambda: dns(status_frames, teks[5]))
        butDNS5.grid(row=4, column=8)

        butDNS6 = tk.Button(event_control, text="L" + str(teks[6] + 1) + "-DNS",
                         command=lambda: dns(status_frames, teks[6]))
        butDNS6.grid(row=4, column=9)

        butDNS7 = tk.Button(event_control, text="L" + str(teks[7] + 1) + "-DNS",
                         command=lambda: dns(status_frames, teks[7]))
        butDNS7.grid(row=4, column=10)

        butResultAkhir = tk.Button(event_control, text="Get Result",
                                command=lambda: replace_ranked(ranked_names, name_frames, ranked_times, status_frames,
                                                               ranked_teams, team_frames, lane_frames))
        butResultAkhir.grid(row=4)

        butOfficiel = tk.Button(event_control, text="Change Official", command=self.changeOfficial)
        butOfficiel.grid(row=5)
        #
        # butFinal = tk.Button(controlPanel, text="Get Final for this Event", command=get_final)
        # butFinal.grid(row=6)

        but_event_display= tk.Button(event_control, text="Change View to Event",
                                     command=lambda: controller.show_frame("Display"))
        but_event_display.grid(row=7)

        ############################

        but_final_display = tk.Button(final_announcement_control, text="Change view to Q Announcement",
                                    command=lambda: controller.show_frame("FinalDisplay"))
        but_final_display.grid(row=1)

        ############################

        but_medal_display = tk.Button(medal_control, text="Change view to Medal",
                                    command=lambda: controller.show_frame("MedalDisplay"))
        but_medal_display.grid(row=5)

        ############################

        temp_img_1_path = "../imgs/logosss.png"
        temp_img_1 = Image.open(temp_img_1_path)
        img_photo_1 = ImageTk.PhotoImage(temp_img_1)
        img_prev_1 = tk.Label(image_control, image=img_photo_1)
        img_prev_1.image = img_photo_1
        img_prev_1.grid(row=0,column=0)

        but_image_display = tk.Button(image_control, text="Change view to Image",
                                      command=lambda: self.update_display_image(temp_img_1_path))
        but_image_display.grid(row=0)

        ############################

        but_announce_display = tk.Button(announcement_control, text="Change view to Announcement",
                                      command=lambda: controller.show_frame("AnnounceDisplay"))
        but_announce_display.grid(row=1)

        ############################

    def changeOfficial(self):
        global title_official
        temp=title_official['text']

        if temp == "":
            title_official['text']="UNOFFICIAL"
        elif temp=="UNOFFICIAL":
            title_official['text']="OFFICIAL"
            print 1
        elif temp=="OFFICIAL":
            title_official['text']="UNOFFICIAL"
            print 2

    def update_display_image(self,path):
        global image_label
        temp = Image.open(path)
        print temp.size
        img2 = ImageTk.PhotoImage(temp)
        image_label.configure(image=img2)
        image_label.image = img2
        self.controller.show_frame("ImageDisplay")

class FinalAnnounceDisplay(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Go to Finals", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

class MedalDisplay(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Medal", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

class ImageDisplay(tk.Frame):

    def __init__(self, parent, controller):
        global image_label
        tk.Frame.__init__(self, parent)
        self.controller = controller
        image_label = tk.Label(self, text="Image",image="")
        image_label.pack(side="top", fill="x", pady=10)

class AnnounceDisplay(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Announcement", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

if __name__ == "__main__":
    app = App()
    app.mainloop()