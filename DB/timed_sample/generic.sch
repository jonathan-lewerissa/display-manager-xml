101;Men 400 LC Meter Freestyle;4;M;0;109;400;A;I;F
102;Women 400 LC Meter Freestyle;4;F;0;109;400;A;I;F
103;Boys 8 & Under 50 LC Meter Butterfly;1;M;0;8;50;D;I;F
104;Girls 8 & Under 50 LC Meter Butterfly;1;F;0;8;50;D;I;F
105;Boys 9-10 50 LC Meter Butterfly;1;M;9;10;50;D;I;F
106;Girls 9-10 50 LC Meter Butterfly;1;F;9;10;50;D;I;F
107;Boys 11-12 50 LC Meter Butterfly;1;M;11;12;50;D;I;F
108;Girls 11-12 50 LC Meter Butterfly;1;F;11;12;50;D;I;F
109;Boys 13-14 50 LC Meter Butterfly;1;M;13;14;50;D;I;F
110;Girls 13-14 50 LC Meter Butterfly;1;F;13;14;50;D;I;F
111;Boys 15-17 50 LC Meter Butterfly;1;M;15;17;50;D;I;F
112;Girls 15-17 50 LC Meter Butterfly;1;F;15;17;50;D;I;F
113;Boys 11-12 200 LC Meter Breaststroke;2;M;11;12;200;C;I;F
114;Girls 11-12 200 LC Meter Breaststroke;2;F;11;12;200;C;I;F
115;Boys 13-14 200 LC Meter Breaststroke;2;M;13;14;200;C;I;F
116;Girls 13-14 200 LC Meter Breaststroke;2;F;13;14;200;C;I;F
117;Boys 15-17 200 LC Meter Breaststroke;2;M;15;17;200;C;I;F
118;Girls 15-17 200 LC Meter Breaststroke;2;F;15;17;200;C;I;F
119;Boys 9-10 100 LC Meter Backstroke;1;M;9;10;100;B;I;F
120;Girls 9-10 100 LC Meter Backstroke;1;F;9;10;100;B;I;F
121;Boys 11-12 100 LC Meter Backstroke;1;M;11;12;100;B;I;F
122;Girls 11-12 100 LC Meter Backstroke;1;F;11;12;100;B;I;F
123;Boys 13-14 100 LC Meter Backstroke;1;M;13;14;100;B;I;F
124;Girls 13-14 100 LC Meter Backstroke;1;F;13;14;100;B;I;F
125;Boys 15-17 100 LC Meter Backstroke;1;M;15;17;100;B;I;F
126;Girls 15-17 100 LC Meter Backstroke;1;F;15;17;100;B;I;F
127;Boys 9-10 100 LC Meter Freestyle;1;M;9;10;100;A;I;F
128;Girls 9-10 100 LC Meter Freestyle;1;F;9;10;100;A;I;F
129;Boys 11-12 100 LC Meter Freestyle;1;M;11;12;100;A;I;F
130;Girls 11-12 100 LC Meter Freestyle;1;F;11;12;100;A;I;F
131;Boys 13-14 100 LC Meter Freestyle;1;M;13;14;100;A;I;F
132;Girls 13-14 100 LC Meter Freestyle;1;F;13;14;100;A;I;F
133;Boys 15-17 100 LC Meter Freestyle;1;M;15;17;100;A;I;F
134;Girls 15-17 100 LC Meter Freestyle;1;F;15;17;100;A;I;F
135;Boys 9-10 200 LC Meter IM;2;M;9;10;200;E;I;F
136;Girls 9-10 200 LC Meter IM;2;F;9;10;200;E;I;F
137;Boys 11-12 200 LC Meter IM;2;M;11;12;200;E;I;F
138;Girls 11-12 200 LC Meter IM;2;F;11;12;200;E;I;F
139;Boys 13-14 200 LC Meter IM;2;M;13;14;200;E;I;F
140;Girls 13-14 200 LC Meter IM;2;F;13;14;200;E;I;F
141;Boys 15-17 200 LC Meter IM;2;M;15;17;200;E;I;F
142;Girls 15-17 200 LC Meter IM;2;F;15;17;200;E;I;F
201;Boys 11-12 200 LC Meter Freestyle;2;M;11;12;200;A;I;F
202;Girls 11-12 200 LC Meter Freestyle;2;F;11;12;200;A;I;F
203;Boys 13-14 200 LC Meter Freestyle;2;M;13;14;200;A;I;F
204;Girls 13-14 200 LC Meter Freestyle;2;F;13;14;200;A;I;F
205;Boys 15-17 200 LC Meter Freestyle;2;M;15;17;200;A;I;F
206;Girls 15-17 200 LC Meter Freestyle;2;F;15;17;200;A;I;F
207;Boys 9-10 100 LC Meter Breaststroke;1;M;9;10;100;C;I;F
208;Girls 9-10 100 LC Meter Breaststroke;1;F;9;10;100;C;I;F
209;Boys 11-12 100 LC Meter Breaststroke;1;M;11;12;100;C;I;F
210;Girls 11-12 100 LC Meter Breaststroke;1;F;11;12;100;C;I;F
211;Boys 13-14 100 LC Meter Breaststroke;1;M;13;14;100;C;I;F
212;Girls 13-14 100 LC Meter Breaststroke;1;F;13;14;100;C;I;F
213;Boys 15-17 100 LC Meter Breaststroke;1;M;15;17;100;C;I;F
214;Girls 15-17 100 LC Meter Breaststroke;1;F;15;17;100;C;I;F
215;Boys 9-10 100 LC Meter Butterfly;1;M;9;10;100;D;I;F
216;Girls 9-10 100 LC Meter Butterfly;1;F;9;10;100;D;I;F
217;Boys 11-12 100 LC Meter Butterfly;1;M;11;12;100;D;I;F
218;Girls 11-12 100 LC Meter Butterfly;1;F;11;12;100;D;I;F
219;Boys 13-14 100 LC Meter Butterfly;1;M;13;14;100;D;I;F
220;Girls 13-14 100 LC Meter Butterfly;1;F;13;14;100;D;I;F
221;Boys 15-17 100 LC Meter Butterfly;1;M;15;17;100;D;I;F
222;Girls 15-17 100 LC Meter Butterfly;1;F;15;17;100;D;I;F
223;Boys 8 & Under 50 LC Meter Backstroke;1;M;0;8;50;B;I;F
224;Girls 8 & Under 50 LC Meter Backstroke;1;F;0;8;50;B;I;F
225;Boys 9-10 50 LC Meter Backstroke;1;M;9;10;50;B;I;F
226;Girls 9-10 50 LC Meter Backstroke;1;F;9;10;50;B;I;F
227;Boys 11-12 50 LC Meter Backstroke;1;M;11;12;50;B;I;F
228;Girls 11-12 50 LC Meter Backstroke;1;F;11;12;50;B;I;F
229;Boys 13-14 50 LC Meter Backstroke;1;M;13;14;50;B;I;F
230;Girls 13-14 50 LC Meter Backstroke;1;F;13;14;50;B;I;F
231;Boys 15-17 50 LC Meter Backstroke;1;M;15;17;50;B;I;F
232;Girls 15-17 50 LC Meter Backstroke;1;F;15;17;50;B;I;F
233;Boys 6-10 50 LC Meter Freestyle Relay;1;M;6;10;50;A;R;F
234;Girls 6-10 50 LC Meter Freestyle Relay;1;F;6;10;50;A;R;F
235;Boys 11-12 100 LC Meter Freestyle Relay;1;M;11;12;100;A;R;F
236;Girls 11-12 100 LC Meter Freestyle Relay;1;F;11;12;100;A;R;F
237;Boys 13-14 100 LC Meter Freestyle Relay;1;M;13;14;100;A;R;F
238;Girls 13-14 100 LC Meter Freestyle Relay;1;F;13;14;100;A;R;F
239;Boys 15-17 100 LC Meter Freestyle Relay;1;M;15;17;100;A;R;F
240;Girls 15-17 100 LC Meter Freestyle Relay;1;F;15;17;100;A;R;F
241;Women 800 LC Meter Freestyle;8;F;0;109;800;A;I;F
242;Men 1500 LC Meter Freestyle;15;M;0;109;1500;A;I;F
301;Men 400 LC Meter IM;4;M;0;109;400;E;I;F
302;Women 400 LC Meter IM;4;F;0;109;400;E;I;F
303;Boys 11-12 200 LC Meter Backstroke;2;M;11;12;200;B;I;F
304;Girls 11-12 200 LC Meter Backstroke;2;F;11;12;200;B;I;F
305;Boys 13-14 200 LC Meter Backstroke;2;M;13;14;200;B;I;F
306;Girls 13-14 200 LC Meter Backstroke;2;F;13;14;200;B;I;F
307;Boys 15-17 200 LC Meter Backstroke;2;M;15;17;200;B;I;F
308;Girls 15-17 200 LC Meter Backstroke;2;F;15;17;200;B;I;F
309;Boys 8 & Under 50 LC Meter Breaststroke;1;M;0;8;50;C;I;F
310;Girls 8 & Under 50 LC Meter Breaststroke;1;F;0;8;50;C;I;F
311;Boys 9-10 50 LC Meter Breaststroke;1;M;9;10;50;C;I;F
312;Girls 9-10 50 LC Meter Breaststroke;1;F;9;10;50;C;I;F
313;Boys 11-12 50 LC Meter Breaststroke;1;M;11;12;50;C;I;F
314;Girls 11-12 50 LC Meter Breaststroke;1;F;11;12;50;C;I;F
315;Boys 13-14 50 LC Meter Breaststroke;1;M;13;14;50;C;I;F
316;Girls 13-14 50 LC Meter Breaststroke;1;F;13;14;50;C;I;F
317;Boys 15-17 50 LC Meter Breaststroke;1;M;15;17;50;C;I;F
318;Girls 15-17 50 LC Meter Breaststroke;1;F;15;17;50;C;I;F
319;Boys 11-12 200 LC Meter Butterfly;2;M;11;12;200;D;I;F
320;Girls 11-12 200 LC Meter Butterfly;2;F;11;12;200;D;I;F
321;Boys 13-14 200 LC Meter Butterfly;2;M;13;14;200;D;I;F
322;Girls 13-14 200 LC Meter Butterfly;2;F;13;14;200;D;I;F
323;Boys 15-17 200 LC Meter Butterfly;2;M;15;17;200;D;I;F
324;Girls 15-17 200 LC Meter Butterfly;2;F;15;17;200;D;I;F
325;Boys 8 & Under 50 LC Meter Freestyle;1;M;0;8;50;A;I;F
326;Girls 8 & Under 50 LC Meter Freestyle;1;F;0;8;50;A;I;F
327;Boys 9-10 50 LC Meter Freestyle;1;M;9;10;50;A;I;F
328;Girls 9-10 50 LC Meter Freestyle;1;F;9;10;50;A;I;F
329;Boys 11-12 50 LC Meter Freestyle;1;M;11;12;50;A;I;F
330;Girls 11-12 50 LC Meter Freestyle;1;F;11;12;50;A;I;F
331;Boys 13-14 50 LC Meter Freestyle;1;M;13;14;50;A;I;F
332;Girls 13-14 50 LC Meter Freestyle;1;F;13;14;50;A;I;F
333;Boys 15-17 50 LC Meter Freestyle;1;M;15;17;50;A;I;F
334;Girls 15-17 50 LC Meter Freestyle;1;F;15;17;50;A;I;F
335;Boys 6-10 50 LC Meter Medley Relay;1;M;6;10;50;E;R;F
336;Girls 6-10 50 LC Meter Medley Relay;1;F;6;10;50;E;R;F
337;Boys 11-12 100 LC Meter Medley Relay;1;M;11;12;100;E;R;F
338;Girls 11-12 100 LC Meter Medley Relay;1;F;11;12;100;E;R;F
339;Boys 13-14 100 LC Meter Medley Relay;1;M;13;14;100;E;R;F
340;Girls 13-14 100 LC Meter Medley Relay;1;F;13;14;100;E;R;F
341;Boys 15-17 100 LC Meter Medley Relay;1;M;15;17;100;E;R;F
342;Girls 15-17 100 LC Meter Medley Relay;1;F;15;17;100;E;R;F
