101;Men 400 LC Meter Freestyle;4;M;0;109;400;A;I;F
102;Women 400 LC Meter Freestyle;4;F;0;109;400;A;I;F
103;Boys 8 & Under 50 LC Meter Butterfly;1;M;0;8;50;D;I;F
104;Girls 8 & Under 50 LC Meter Butterfly;1;F;0;8;50;D;I;F
105;Boys 9-10 50 LC Meter Butterfly;1;M;9;10;50;D;I;F
106;Girls 9-10 50 LC Meter Butterfly;1;F;9;10;50;D;I;F
107;Boys 11-12 50 LC Meter Butterfly;1;M;11;12;50;D;I;F
108;Girls 11-12 50 LC Meter Butterfly;1;F;11;12;50;D;I;F
109;Boys 13-14 50 LC Meter Butterfly;1;M;13;14;50;D;I;F
110;Girls 13-14 50 LC Meter Butterfly;1;F;13;14;50;D;I;F
111;Boys 15-17 50 LC Meter Butterfly;1;M;15;17;50;D;I;F
112;Girls 15-17 50 LC Meter Butterfly;1;F;15;17;50;D;I;F
113;Boys 11-12 200 LC Meter Breaststroke;2;M;11;12;200;C;I;F
114;Girls 11-12 200 LC Meter Breaststroke;2;F;11;12;200;C;I;F
115;Boys 13-14 200 LC Meter Breaststroke;2;M;13;14;200;C;I;F
116;Girls 13-14 200 LC Meter Breaststroke;2;F;13;14;200;C;I;F
117;Boys 15-17 200 LC Meter Breaststroke;2;M;15;17;200;C;I;F
118;Girls 15-17 200 LC Meter Breaststroke;2;F;15;17;200;C;I;F
119;Boys 9-10 100 LC Meter Backstroke;1;M;9;10;100;B;I;F
120;Girls 9-10 100 LC Meter Backstroke;1;F;9;10;100;B;I;F
121;Boys 11-12 100 LC Meter Backstroke;1;M;11;12;100;B;I;F
122;Girls 11-12 100 LC Meter Backstroke;1;F;11;12;100;B;I;F
123;Boys 13-14 100 LC Meter Backstroke;1;M;13;14;100;B;I;F
124;Girls 13-14 100 LC Meter Backstroke;1;F;13;14;100;B;I;F
125;Boys 15-17 100 LC Meter Backstroke;1;M;15;17;100;B;I;F
126;Girls 15-17 100 LC Meter Backstroke;1;F;15;17;100;B;I;F
127;Boys 9-10 100 LC Meter Freestyle;1;M;9;10;100;A;I;F
128;Girls 9-10 100 LC Meter Freestyle;1;F;9;10;100;A;I;F
129;Boys 11-12 100 LC Meter Freestyle;1;M;11;12;100;A;I;F
130;Girls 11-12 100 LC Meter Freestyle;1;F;11;12;100;A;I;F
131;Boys 13-14 100 LC Meter Freestyle;1;M;13;14;100;A;I;F
132;Girls 13-14 100 LC Meter Freestyle;1;F;13;14;100;A;I;F
133;Boys 15-17 100 LC Meter Freestyle;1;M;15;17;100;A;I;F
134;Girls 15-17 100 LC Meter Freestyle;1;F;15;17;100;A;I;F
135;Boys 9-10 200 LC Meter IM;2;M;9;10;200;E;I;F
136;Girls 9-10 200 LC Meter IM;2;F;9;10;200;E;I;F
137;Boys 11-12 200 LC Meter IM;2;M;11;12;200;E;I;F
138;Girls 11-12 200 LC Meter IM;2;F;11;12;200;E;I;F
139;Boys 13-14 200 LC Meter IM;2;M;13;14;200;E;I;F
140;Girls 13-14 200 LC Meter IM;2;F;13;14;200;E;I;F
141;Boys 15-17 200 LC Meter IM;2;M;15;17;200;E;I;F
142;Girls 15-17 200 LC Meter IM;2;F;15;17;200;E;I;F