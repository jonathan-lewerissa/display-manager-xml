from yattag import Doc, indent
import sqlite3
from lxml import etree
import pyodbc
import datetime

directory = 'xml/'


def import_event_list(generic_file):
    generic = open(generic_file, 'r')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    cursor.execute('DELETE FROM EventList')
    cursor.execute('VACUUM')

    with generic as events:
        for event in events:
            data = str(event).rstrip().split(';')
            kueri = '''INSERT INTO EventList VALUES (?,?,?,?,?,?,?,?)'''
            param = (
            str(data[0]), str(data[1]), str(data[2]), str(data[3]), str(data[6]), str(data[9]), str(""), str(""))
            cursor.execute(kueri, param)

    db.commit()
    db.close()


def import_start_list(file):
    startlist = open(file, 'r')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    cursor.execute('DELETE FROM StartList')
    cursor.execute('VACUUM')

    with startlist as competitors:
        for competitor in competitors:
            data = str(competitor).rstrip().split(';')
            if data[0] != '':
                event = [str(data[0]), str(data[1]), str(data[2]), str(data[3])]
            else:
                kueri = '''INSERT INTO StartList VALUES (?,?,?,?,?,?,?,?,?,?,?)'''
                print(data)
                param = (
                str(event[0]), str(event[2]), str(event[1]), str(data[1]), str(data[2]), str(data[3]), str(data[4]),
                str(data[5]), str(data[6]), str(data[7]), str(""))
                cursor.execute(kueri, param)

    db.commit()
    db.close()


def import_nrmr():
    conn_str = (
        r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
        r'DBQ=C:\display-manager-xml\DB\PIAG_2018_Database.mdb;'
        r'PWD=TeP69s)lAd_mW-(J_72u'
    )
    mscnxn = pyodbc.connect(conn_str, readonly=True)
    mscur = mscnxn.cursor()

    mscur.execute(
        'SELECT Event.Event_no,RecordsbyEvent.tag_ptr,RecordsbyEvent.Record_Time FROM RecordsbyEvent LEFT JOIN Event ON Event.Event_ptr = RecordsbyEvent.event_ptr WHERE RecordsbyEvent.tag_ptr=1 ORDER BY RecordsbyEvent.event_ptr')
    rows = mscur.fetchall()

    mscnxn.close()

    db = sqlite3.connect('local.db')
    cur = db.cursor()

    for row in rows:
        time = datetime.timedelta(seconds=row[2])
        h, m, s = str(time).split(':')
        time = str(m) + ':' + str(s[:5])
        query = 'UPDATE EventList SET even_nr="' + str(time) + '" WHERE even_number="' + str(row[0]) + '"'
        cur.execute(query)
    db.commit()
    db.close()

    #########################################

    mscnxn = pyodbc.connect(conn_str, readonly=True)
    mscur = mscnxn.cursor()

    mscur.execute(
        'SELECT Event.Event_no,RecordsbyEvent.tag_ptr,RecordsbyEvent.Record_Time FROM RecordsbyEvent LEFT JOIN Event ON Event.Event_ptr = RecordsbyEvent.event_ptr WHERE RecordsbyEvent.tag_ptr=2 ORDER BY RecordsbyEvent.event_ptr')
    rows = mscur.fetchall()

    mscnxn.close()

    db = sqlite3.connect('local.db')
    cur = db.cursor()

    for row in rows:
        time = datetime.timedelta(seconds=row[2])
        h, m, s = str(time).split(':')
        time = str(m) + ':' + str(s[:5])
        query = 'UPDATE EventList SET even_mr="' + str(time) + '" WHERE even_number="' + str(row[0]) + '"'
        cur.execute(query)
    db.commit()
    db.close()


def write_to_file(path, attr):
    global directory
    file = open(directory + path, 'w')
    file.write(attr)
    file.close()


def generate_event_list():
    doc, tag, text = Doc().tagtext()

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    kueri = '''SELECT * FROM EventList'''
    cursor.execute(kueri)

    events = cursor.fetchall()
    db.close()

    print(len(events))

    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')
    with tag('EVENTLIST'):
        with tag('DATE'):
            with tag('DAY'):
                text(str(1))
            with tag('MONTH'):
                text(str(1))
            with tag('YEAR'):
                text(str(2018))
        number = 1
        for data in events:
            with tag('EVEN'):
                with tag('NO'):
                    text(str(number))
                    number += 1
                with tag('EVEN_TIME'):
                    text(str())
                with tag('EVEN_NUMBER'):
                    text(data[0])
                with tag('EVEN_NAME'):
                    text(data[1])
                with tag('EVEN_STATUS'):
                    if data[-1] == 'P':
                        status = 'Preliminary'
                    else:
                        status = 'Final'
                    text(status)
                with tag('EVEN_EVEN'):
                    text(data[1] + ' - ' + status)
    write_to_file('EventList.xml', indent(doc.getvalue()))


def generate_split(competitors):
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    var = competitors[0]
    print(var)

    kueri = '''SELECT even_name FROM EventList WHERE even_number=? and even_type=?'''
    param = (str(var[0]), str(var[1]))
    cursor.execute(kueri, param)

    rows = cursor.fetchall()
    row = rows[0]

    db.close()

    with tag('HEAT'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str(var[0]))
            with tag('EVENT_HEAT_NO'):
                text(str(var[2]))
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NAME'):
                text(str(row[0]))
            with tag('EVENT_EVEN'):
                text(str(row[0]))

        for i in range(3):
            with tag('COMPETITOR'):
                with tag('COMPETITOR_RANK'):
                    text(str(i + 1))
                with tag('COMPETITOR_NAME'):
                    text(str())
                with tag('COMPETITOR_LANE'):
                    text(str())
                with tag('COMPETITOR_SPLIT_TIME'):
                    text(str())
                with tag('COMPETITOR_SPLIT_PLUS'):
                    text(str())
                with tag('COMPETITOR_NATION'):
                    text(str())

    write_to_file('Split.xml', indent(doc.getvalue()))


def generate_current_result(competitors):
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    var = competitors[0]

    kueri = '''SELECT even_name FROM EventList WHERE even_number=? and even_type=?'''
    param = (str(var[0]), str(var[1]))
    cursor.execute(kueri, param)

    rows = cursor.fetchall()
    row = rows[0]

    db.close()

    with tag('HEAT'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str(var[0]))
            with tag('EVENT_HEAT_NO'):
                text(str(var[2]))
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NAME'):
                text(str(row[0]))
            with tag('EVENT_EVEN'):
                text(str(row[0]))

        for i in range(1, 9):
            with tag('COMPETITOR'):
                with tag('COMPETITOR_ID'):
                    text(str())
                with tag('COMPETITOR_LANE'):
                    text(str())
                with tag('COMPETITOR_NAME'):
                    text(str())
                with tag('COMPETITOR_FIRSTNAME'):
                    text(str())
                with tag('COMPETITOR_LASTNAME'):
                    text(str())
                with tag('COMPETITOR_TEAMNAME'):
                    text(str())
                with tag('COMPETITOR_NATION'):
                    text(str())
                with tag('COMPETITOR_RESULT'):
                    text(str())
                with tag('COMPETITOR_SPLIT'):
                    text(str())
                with tag('COMPETITOR_RANK'):
                    text(str(i))
                with tag('COMPETITOR_STATUS'):
                    text(str())
                with tag('COMPETITOR_FLAG'):
                    text(str())
    write_to_file('CurrentResult.xml', indent(doc.getvalue()))


def generate_current_heat(competitors):
    global directory
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    var = competitors[0]

    kueri = '''SELECT even_name FROM EventList WHERE even_number=? and even_type=?'''
    param = (str(var[0]), str(var[1]))
    cursor.execute(kueri, param)

    rows = cursor.fetchall()
    row = rows[0]

    print type(rows)
    print type(row[0])
    print row[0]

    db.close()

    with tag('HEAT'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str(var[0]))
            with tag('EVENT_HEAT_NO'):
                text(str(var[2]))
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NAME'):
                text(str(row[0]))
            with tag('EVENT_EVEN'):
                text(str(row[0]))

        for i in range(1, 9):
            with tag('COMPETITOR'):
                with tag('COMPETITOR_ID'):
                    text(str())
                with tag('COMPETITOR_LANE'):
                    text(str(i))
                with tag('COMPETITOR_NAME'):
                    text(str())
                with tag('COMPETITOR_FIRSTNAME'):
                    text(str())
                with tag('COMPETITOR_LASTNAME'):
                    text(str())
                with tag('COMPETITOR_SEEDED'):
                    text(str())
                with tag('COMPETITOR_TEAMNAME'):
                    text(str())
                with tag('COMPETITOR_NATION'):
                    text(str())
                with tag('COMPETITOR_FLAG'):
                    text(str())

    write_to_file('CurrentHeat.xml', indent(doc.getvalue()))

    doc = etree.parse(directory + 'CurrentHeat.xml')
    root = doc.getroot()

    for competitor in competitors:
        aq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_ID')
        aq[0].text = str(competitor[3])
        bq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_NAME')
        bq[0].text = str(competitor[5] + ' ' + competitor[6])
        cq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_FIRSTNAME')
        cq[0].text = str(competitor[5])
        dq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_LASTNAME')
        dq[0].text = str(competitor[6])
        eq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_SEEDED')
        eq[0].text = str(competitor[8])
        fq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_TEAMNAME')
        fq[0].text = str(competitor[8])
        fq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_NATION')
        fq[0].text = str(competitor[8])
        fq = root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE=' + str(competitor[4]) + ']/COMPETITOR_FLAG')
        fq[0].text = str(competitor[7])

        etree.ElementTree(root).write(directory + 'CurrentHeat.xml', pretty_print=True)


def generate_next_heat(competitors):
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    db = sqlite3.connect('local.db')
    cursor = db.cursor()

    var = competitors[0]

    kueri = '''SELECT even_name FROM EventList WHERE even_number=? and even_type=?'''
    param = (str(var[0]), str(var[1]))
    cursor.execute(kueri, param)

    rows = cursor.fetchall()
    row = rows[0]

    db.close()

    with tag('HEAT'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str(var[0]))
            with tag('EVENT_HEAT_NO'):
                text(str(var[2]))
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NAME'):
                text(str(row[0]))
            with tag('EVENT_EVEN'):
                text(str(row[0]))

        for i in range(0, 8):
            try:
                competitor = competitors[i]
            except:
                competitor = ['', '', '', '', '', '', '', '', '']
            with tag('COMPETITOR'):
                with tag('COMPETITOR_ID'):
                    text(str(competitor[3]))
                with tag('COMPETITOR_LANE'):
                    text(str(competitor[4]))
                with tag('COMPETITOR_NAME'):
                    text(str(competitor[5]) + ' ' + str(competitor[6]))
                with tag('COMPETITOR_FIRSTNAME'):
                    text(str(competitor[5]))
                with tag('COMPETITOR_LASTNAME'):
                    text(str(competitor[6]))
                with tag('COMPETITOR_SEEDED'):
                    text(str(competitor[8]))
                with tag('COMPETITOR_TEAMNAME'):
                    text(str(competitor[8]))
                with tag('COMPETITOR_NATION'):
                    text(str(competitor[8]))
                    text(str())
                with tag('COMPETITOR_FLAG'):
                    text(str(competitor[7]))
    write_to_file('NextHeat.xml', indent(doc.getvalue()))


def generate_medals():
    doc, tag, text = Doc().tagtext()

    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')
    with tag('MEDAL'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str())
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NAME'):
                text(str())
            with tag('EVENT_EVEN'):
                text(str())

        for i in range(3):
            with tag('COMPETITOR'):
                with tag('COMPETITOR_RANK'):
                    text(str())
                with tag('COMPETITOR_ID'):
                    text(str())
                with tag('COMPETITOR_LANE'):
                    text(str())
                with tag('COMPETITOR_NAME'):
                    text(str())
                with tag('COMPETITOR_FIRSTNAME'):
                    text(str())
                with tag('COMPETITOR_LASTNAME'):
                    text(str())
                with tag('COMPETITOR_TEAMNAME'):
                    text(str())
                with tag('COMPETITOR_NATION'):
                    text(str())
                with tag('COMPETITOR_RESULT'):
                    text(str())
                with tag('COMPETITOR_FLAG'):
                    text(str())
    write_to_file('Medals.xml', indent(doc.getvalue()))


def generate_running_time():
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    with tag('TIMING'):
        text(str())

    write_to_file('Timing.xml', indent(doc.getvalue()))


# Function to generate xml in the event of false start
# Function must have a list of dictionaries as argument
def generate_false(competitor_list):
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    with tag('HEAT'):
        with tag('EVENT'):
            with tag('EVENT_NO'):
                text(str())
            with tag('EVENT_HEAT_NO'):
                text(str())
            with tag('EVENT_STATUS'):
                text(str())
            with tag('EVENT_NO'):
                text(str())
        for competitor in competitor_list:
            with tag('COMPETITOR'):
                with tag('COMPETITOR_ID'):
                    text(str(competitor['id']))
                with tag('COMPETITOR_LANE'):
                    text(str(competitor['lane']))
                with tag('COMPETITOR_FIRSTNAME'):
                    text(str(competitor['firstname']))
                with tag('COMPETITOR_LASTNAME'):
                    text(str(competitor['lastname']))
                with tag('COMPETITOR_TEAMNAME'):
                    text(str(competitor['teamname']))
                with tag('COMPETITOR_NATION'):
                    text(str(competitor['nation']))
                with tag('COMPETITOR_STATUS'):
                    text(str(competitor['status']))
    write_to_file('False.xml', indent(doc.getvalue()))


def generate_reaction_time():
    doc, tag, text = Doc().tagtext()
    doc.asis('<?xml version="1.0" encoding="UTF-8"?>')

    with tag('HEAT'):
        for i in range(10):
            with tag('REACTION'):
                with tag('LANE'):
                    text(str(i))
                with tag('TIME'):
                    text(str())

    write_to_file('Reaction.xml', indent(doc.getvalue()))


if __name__ == "__main__":
    import_event_list('sample/generic.sch')
    import_start_list('sample/startlist.slx')
    import_nrmr()
# generate_event_list()
# generate_reaction_time()
# generate_current_heat([['115','F','1','21','4','a','no','ina','indonesia','']])
