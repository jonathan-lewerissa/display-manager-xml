import Tkinter as tk
import tkFont as tkfont
import ttk
from time import sleep
from threading import Thread
import socket, sys
from struct import *
import datetime
import sqlite3
from lxml import etree
from manager_xml import *
from update_xml import *
from multiprocessing import Process
import thread
from time import sleep
from PIL import Image, ImageTk
from random import randint
import os, sys

# Global Var
app = None
counter = 0.0
minutes = 0
time_string = ""
n_iter = 8
s = 0
e = 8
s1 = 0
e1 = 8
first = 0.0
event_number = 1
del_list = []
del_list1 = []
res_del_list = []
res_del_list1 = []
dq_del_list = []
queryyy = None
heatt = None
mode_lomba = None
pesertas = None
name_labels_frames = []
teams = []
heat_ = None
laps = None
split_counter = 0
total_competitor = 0
competitor_split_count = {}
prev_time_catch = {}
ranked_names = []
ranked_times = []
ranked_lanes = []
ranked_teams = []
pesertas_number = 0
counter_pesertas = 0
replaced_names = []
replaced_times = []
lane_labels = []
nameeee = {}
teamnamee = {}
title_something = None
replaced_teams = []
replaced_lanes = []
dns_dict = {}
timer_now = None
batas_lane = 8
h_comp = 65
def_w = 1366
image_display_path = None
title_official = None
image_label = None
medal_image_label = None
container_medal = None
label_final = None
app_container = None
final_display_frame = None
container_medal_result = None
final_draw_flag = None
looping_start = 0
c_panel_names = []
dq_del_cp = []
# batas_lane=10
# h_comp=50
# Global Var
puyo = 1
colors = ['#FFDF00', '#C0C0C0', '#cd7f32']
row_record = None
record_flag = None
recorded_time = []


def listener(home, label, status_label, pos_label, name_frames):
    global first, nameeee, teamnamee, puyo, dns_dict
    global laps, timer_now
    global competitor_split_count, name_labels_frames, pesertas_number, counter_pesertas
    global ranked_names, ranked_times, ranked_lanes, ranked_teams
    global row_record, record_flag, recorded_time
    # create an INET, STREAMing socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
        s.bind(('', 10000))
    except socket.error as msg:
        print('Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()

    # receive a packet
    while True:
        try:
            packet = s.recvfrom(65565)

            # packet string from tuple
            packet = packet[0]

            # take first 20 characters for the ip header
            ip_header = packet[0:20]

            # now unpack them :)
            iph = unpack('!BBHHHBBH4s4s', ip_header)

            version_ihl = iph[0]
            version = version_ihl >> 4
            ihl = version_ihl & 0xF

            iph_length = ihl * 4

            # ttl = iph[5]
            # protocol = iph[6]
            # s_addr = socket.inet_ntoa(iph[8])
            # d_addr = socket.inet_ntoa(iph[9])

            # print 'Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(s_addr) + ' Destination Address : ' + str(d_addr)

            tcp_header = packet[iph_length:iph_length + 20]

            # now unpack them :)
            tcph = unpack('!HHLLBBHHH', tcp_header)

            # source_port = tcph[0]
            # dest_port = tcph[1]
            # sequence = tcph[2]
            # acknowledgement = tcph[3]
            doff_reserved = tcph[4]
            tcph_length = doff_reserved >> 4

            # print 'Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Sequence Number : ' + str(sequence) + ' Acknowledgement : ' + str(acknowledgement) + ' TCP header length : ' + str(tcph_length)

            h_size = iph_length + tcph_length * 4
            data_size = len(packet) - h_size

            # get data from the packet
            data = packet[h_size:].split()
        except:
            continue

        if "ReactionTime" in data:
            ind = data.index("ReactionTime")
            print data
            print(data[ind], data[ind + 1])
            react_index = int(data[ind - 1]) - 1
            timeee = float(data[ind + 1]) / float(10000)
            if timeee >= 60.0:
                te = str(datetime.timedelta(seconds=timeee))
                hour, minu, seco = te.split(':')
                te = minu + ':' + seco[:5]
            else:
                te = str(datetime.timedelta(seconds=timeee))
                hour, minu, seco = te.split(':')
                te = seco[:5]

            print(str(te))
            print(str(react_index))
            try:
                update_reaction(react_index + 1, te)
                srr = "+" + str(te)
                lab3 = tk.Label(status_label[react_index], text=srr, bg="black", fg="white",
                                font=("Verdana", 30, "bold"))
                lab3.pack()
                del_list.append(lab3)
                lab3.after(6000, lambda: destroy_label(False))
            except:
                print "cannot update reaction"
            continue
        if "RunningTime" in data:
            ind = data.index("RunningTime")
            # print(data[ind], data[ind + 1])
            timeee = float(data[ind + 1]) / float(10000)
            if timeee < 0.0 or timeee == -0.0:
                timeee = float(0.0)
            if timeee >= 60.0:
                t = str(datetime.timedelta(seconds=timeee))
                hour, minu, seco = t.split(':')
                t = minu + ':' + seco[:5]
            else:
                t = str(datetime.timedelta(seconds=timeee))
                hour, minu, seco = t.split(':')
                t = seco[:5]

            if t == 0.0:
                t == ""
            label['text'] = t
            try:
                update_timing(str(t))
            except:
                print('cannot update timing')
                continue
            continue
        if "LaneTime" in data:

            ind = data.index("LaneTime")
            lane_index = int(data[ind - 1]) - 1

            print data
            print(data[ind], lane_index, data[ind + 1])

            if int(data[ind + 1]) < 0:
                label['text'] = 0.0
                continue
            # print 'lane index:' + str(lane_index)
            if lane_index < 0:
                continue
            try:
                if prev_time_catch[lane_index] == data[ind + 1]:
                    continue
                prev_time_catch[lane_index] = data[ind + 1]
            except:
                print "no prev time catch"

            if lane_index < 0 or lane_index > 7:
                # lane_index=0
                continue
            rank = data[ind + 3]
            # print("rank : " + str(rank))
            # print(type(rank))

            lappy = int(data[ind + 5])

            timeee = float(data[ind + 1]) / float(10000)
            # print(timeee)
            if rank == '1':
                # print('1')
                first = timeee
                splitting_time = str(datetime.timedelta(seconds=first))
                # print splitting_time
                spl_h, spl_m, spl_s = splitting_time.split(":")
                splitting_time = str(spl_m) + ":" + str(spl_s[:5])
                # print splitting_time
            else:
                # print('beda')
                # print first
                splitting_time = str(datetime.timedelta(seconds=timeee - first))
                spl_h, spl_m, spl_s = splitting_time.split(":")
                splitting_time = "+" + str(spl_m) + ":" + str(spl_s[:5])
                # print splitting_time
            t = str(datetime.timedelta(seconds=timeee))
            hour, minu, seco = t.split(':')
            t = minu + ':' + seco[:5]

            # print(str(lane_index))
            update_split(str(lane_index + 1), str(rank), str(t), str(splitting_time))

            if rank == '1' and int(lappy) >= int(laps) and record_flag is None:
                # check if break national record
                try:
                    if str(t) < str(row_record[0]):
                        print "new national record!!"
                        rank = "1-NR"
                        record_flag = "NR"
                    elif str(t) < str(row_record[1]):
                        print "new meet record!!"
                        rank = "1-MR"
                        record_flag = "MR"
                except:
                    print "cannot show record"

            to_del = status_label[lane_index].winfo_children()
            if to_del:
                for element in to_del:
                    element.destroy()

            lab = tk.Label(status_label[lane_index], text=t, bg="black", fg="white", font=("Verdana", 40, "bold"))
            lab1 = tk.Label(pos_label[lane_index], text=rank, bg="black", fg="white", font=("Verdana", 30, "bold"))
            try:
                temp_cek = dns_dict[lane_index]
            except:
                temp_cek = 0
            if lab != temp_cek:
                lab.pack()
                lab1.pack()
                if int(lappy) < int(laps):
                    del_list.append(lab)
                    del_list1.append(lab1)
                else:
                    res_del_list.append(lab)
                    res_del_list1.append(lab1)

            print(lappy, int(laps))

            try:
                recorded_time[lane_index] = str(t)
            except:
                print "cannot record"

            if int(lappy) < int(laps):
                home.after(6000, lambda: destroy_label(True))
            else:
                counter_pesertas += 1
                # temp = nameeee[lane_index]
                # temp_team = teamnamee[lane_index]
                # ranked_names.append(temp['text'])
                # ranked_teams.append(temp_team['text'])
                ranked_times.append(str(t))
                ranked_lanes.append(lane_index)

                update_current_result(str(lane_index + 1), str(t), puyo)

                puyo += 1
                # print ranked_names
                # print ranked_teams
                # home.after(60000,lambda:destroy_label(True))
                # print counter_pesertas, pesertas_number
                if counter_pesertas == pesertas_number:
                    print('masuk')
                # changeOfficial()
                # for i in range(0,len(ranked_names)):
                # 	update_current_result(ranked_lanes[i],ranked_times[i],str(i+1))
                # home.after(6000,lambda:replace_ranked(ranked_names,name_frames,ranked_times,status_label))


# def dns(status_frames, lane):
#     global dns_dict
#     try:
#         temp = dns_dict[lane]
#     except:
#         print "go"
#     if temp['text'] == "DNS":
#         lab = tk.Label(status_frames[lane], text="", bg="white", fg="black", font=("Veldana", 40, "bold"))
#     else:
#         lab = tk.Label(status_frames[lane], text="DNS", bg="white", fg="black", font=("Veldana", 40, "bold"))
#     dns_dict[lane] = lab
#     lab.pack()


def replace_ranked(name_frames, status_label, team_frames, lane_frames,pos_frames):
    print('replace_ranked')
    global name_labels_frames, lane_labels, teams, del_list1, del_list, dq_del_list
    global replaced_names, replaced_times, replaced_teams, replaced_lanes
    global queryyy, heatt, recorded_time, record_flag
    global ranked_times,ranked_lanes
    global res_del_list1, res_del_list

    db = sqlite3.connect('local.db')
    cur = db.cursor()

    for i in range(0, len(ranked_times)):

        name_check_query = 'SELECT * FROM StartList WHERE competitor_lane=? AND even_number=? AND even_type=? AND even_heat=?'
        cur.execute(name_check_query,[str(ranked_lanes[i]+1),str(queryyy[0]), str(queryyy[2]), str(heatt)])

        name_check = cur.fetchone()

        if name_check:
            time_update_query = 'UPDATE StartList SET competitor_time="' + str(
                ranked_times[i]) + '" WHERE competitor_lane=' + str(ranked_lanes[i]+1) + ' AND even_number=' + str(
                queryyy[0]) + ' AND even_type="' + str(queryyy[2]) + '" AND even_heat=' + str(heatt)
            print time_update_query
            cur.execute(time_update_query)
        else:
            time_update_query = 'INSERT INTO StartList VALUES(?,?,?,?,?,?,?,?,?,?,?)'
            cur.execute(time_update_query,
                        [str(queryyy[0]), str(queryyy[2]), str(heatt), str("-"), str(ranked_lanes[i]+1), str("---"),
                         str("---"), str("-"), str("---"), str(""), str(ranked_times[i])])

    db.commit()
    db.close()

    db = sqlite3.connect('local.db')
    cur = db.cursor()

    results_list = cur.execute(
        'SELECT * FROM StartList WHERE competitor_time!="" AND even_number=? AND even_type=? AND even_heat=? ORDER BY competitor_time',
        (str(queryyy[0]), str(queryyy[2]), str(heatt)))
    results_list = results_list.fetchall()
    print "result list"
    print results_list

    disq_list = cur.execute(
        'SELECT * FROM StartList WHERE competitor_status="DQ" AND even_number=? AND even_type=? AND even_heat=?',
        (str(queryyy[0]), str(queryyy[2]), str(heatt)))
    disq_list = disq_list.fetchall()
    print "disq list"
    print disq_list

    dns_list = cur.execute(
        'SELECT * FROM StartList WHERE competitor_status="DNS" AND even_number=? AND even_type=? AND even_heat=?',
        (str(queryyy[0]), str(queryyy[2]), str(heatt)))
    dns_list = dns_list.fetchall()
    print "dns list"
    print dns_list

    for element in teams:
        element.destroy()
    teams = []
    for element in name_labels_frames:
        element.destroy()
    for element in del_list1:
        element.destroy()
    for element in del_list:
        element.destroy()
    for element in lane_labels:
        element.destroy()
    for element in dq_del_list:
        element.destroy()
    for element in res_del_list:
        element.destroy()
    for element in res_del_list1:
        element.destroy()

    del_list = []
    del_list1 = []
    dq_del_list = []
    res_del_list = []
    res_del_list1 = []
    name_labels_frames = []
    lane_labels = []
    for i in range(0, len(results_list)):
        row = results_list[i]
        res_first_name = row[5]
        res_last_name = row[6]
        res_teams = row[7]
        res_lane = row[4]
        res_time = row[10]
        res_num = str(i + 1)

        if i == 0:
            try:
                if record_flag == "NR":
                    res_num = res_num + '-NR'
                    query = 'UPDATE EventList SET even_nr="' + str(res_time) + '", even_mr="' + str(
                        res_time) + '" WHERE even_number="' + str(queryyy[0]) + '"'
                    cur.execute(query)

                elif record_flag == "MR":
                    res_num = res_num + '-MR'
                    query = 'UPDATE EventList SET even_mr="' + str(res_time) + '" WHERE even_number="' + str(
                        queryyy[0]) + '"'
                    cur.execute(query)
                db.commit()
            except:
                print "cannot "
            db.close()

        temp = tk.Label(lane_frames[i], text=res_num, fg="black", bg="white",
                        font=("Verdana", 25, "bold"))
        lane_labels.append(temp)

        temp = tk.Label(name_frames[i], text=res_last_name + " " + res_first_name, fg="black",
                        bg="yellow",
                        font=("Verdana", 25, "bold"))
        replaced_names.append(temp)

        replaced_times.append(
            tk.Label(status_label[i], text=str(res_time), fg="white", bg="black",
                     font=("Verdana", 35, "bold")))
        replaced_lanes.append(
            tk.Label(pos_frames[i], text="L" + str(res_lane), fg="white", bg="black",
                     font=("Verdana", 25, "bold")))
        replaced_teams.append(
            tk.Label(team_frames[i], text=str(res_teams), fg="#0009ba", bg="white",
                     font=("Verdana", 25, "bold")))

    if disq_list:
        for i in range(0, len(disq_list)):
            row = disq_list[i]
            dis_first_name = row[5]
            dis_last_name = row[6]
            dis_teams = row[7]
            dis_lane = row[4]

            temp = tk.Label(lane_frames[i + len(results_list)], text=str(i + 1 + len(results_list)), fg="black",
                            bg="white",
                            font=("Verdana", 25, "bold"))
            lane_labels.append(temp)

            temp = tk.Label(name_frames[i + len(results_list)], text=dis_last_name + " " + dis_first_name, fg="black",
                            bg="yellow",
                            font=("Verdana", 25, "bold"))
            replaced_names.append(temp)

            replaced_times.append(
                tk.Label(status_label[i + len(results_list)], text="DQ", fg="white", bg="black",
                         font=("Verdana", 35, "bold")))
            replaced_lanes.append(
                tk.Label(pos_frames[i + len(results_list)], text="L" + str(dis_lane), fg="white", bg="black",
                         font=("Verdana", 25, "bold")))
            replaced_teams.append(
                tk.Label(team_frames[i + len(results_list)], text=str(dis_teams), fg="#0009ba", bg="white",
                         font=("Verdana", 25, "bold")))
    if dns_list:
        for i in range(0, len(dns_list)):
            row = dns_list[i]
            dns_first_name = row[5]
            dns_last_name = row[6]
            dns_teams = row[7]
            dns_lane = row[4]

            temp = tk.Label(lane_frames[i + len(results_list) + len(disq_list)],
                            text=str(i + 1 + len(results_list) + len(disq_list)), fg="black",
                            bg="white",
                            font=("Verdana", 30, "bold"))
            lane_labels.append(temp)

            temp = tk.Label(name_frames[i + len(results_list) + len(disq_list)],
                            text=dns_last_name + " " + dns_first_name, fg="black",
                            bg="yellow",
                            font=("Verdana", 25, "bold"))
            replaced_names.append(temp)

            replaced_times.append(
                tk.Label(status_label[i + len(results_list) + len(disq_list)], text="DNS", fg="white", bg="black",
                         font=("Verdana", 35, "bold")))
            replaced_lanes.append(
                tk.Label(pos_frames[i + len(results_list) + len(disq_list)], text="L" + str(dns_lane), fg="white",
                         bg="black",
                         font=("Verdana", 25, "bold")))
            replaced_teams.append(
                tk.Label(team_frames[i + len(results_list) + len(disq_list)], text=str(dns_teams), fg="#0009ba",
                         bg="white",
                         font=("Verdana", 25, "bold")))

    start_ranked = 0
    for element in replaced_names:
        element.pack(side=tk.LEFT)
    for element in lane_labels:
        element.pack()
    for element in replaced_teams:
        element.pack(side=tk.LEFT)
    for element in replaced_times:
        element.pack()
    for element in replaced_lanes:
        element.pack()
    for element in lane_labels:
        element['text'] = str(start_ranked + 1)
        start_ranked += 1
    # for i in range (0,len(lane_labels)):
    # 	temp=lane_labels[i]
    # 	temp['text']=i+1
    replaced_names = []
    replaced_times = []
    replaced_teams = []
    replaced_lanes = []


def disq(status_frames, ind, event_control, col_cp):
    global dq_del_list,dq_del_cp
    lab = tk.Label(status_frames[ind], text="DQ", bg="white", fg="black", font=("Verdana", 40, "bold"))
    lab1 = tk.Label(event_control,text="DQ")
    dq_del_cp.append(lab1)
    lab1.grid(row=7+ind,column=col_cp+1)
    lab.pack()
    dq_del_list.append(lab)

    db = sqlite3.connect('local.db')
    cur = db.cursor()
    time_update_query = 'UPDATE StartList SET competitor_status="DQ" WHERE competitor_lane=' + str(
        int(ind) + 1) + ' AND even_number=' + str(
        queryyy[0]) + ' AND even_type="' + str(queryyy[2]) + '" AND even_heat=' + str(heatt)
    # print time_update_query
    cur.execute(time_update_query)
    db.commit()
    db.close()


def dns(status_frames, ind, event_control, col_cp):
    global dq_del_list,dq_del_cp
    lab = tk.Label(status_frames[ind], text="DNS", bg="white", fg="black", font=("Verdana", 40, "bold"))
    lab1 = tk.Label(event_control,text="DNS")
    dq_del_cp.append(lab1)
    lab1.grid(row=7+ind,column=col_cp+1)
    lab.pack()
    dq_del_list.append(lab)

    db = sqlite3.connect('local.db')
    cur = db.cursor()
    time_update_query = 'UPDATE StartList SET competitor_status="DNS" WHERE competitor_lane=' + str(
        int(ind) + 1) + ' AND even_number=' + str(
        queryyy[0]) + ' AND even_type="' + str(queryyy[2]) + '" AND even_heat=' + str(heatt)
    print time_update_query
    cur.execute(time_update_query)
    db.commit()
    db.close()


def destroy_label(flag):
    global s, e, del_list, del_list1
    if flag == True:
        del_list[0].destroy()
        del_list1[0].destroy()
        del del_list[0]
        del del_list1[0]
    elif flag == False:
        del_list[0].destroy()
        del del_list[0]


def text_toDisplay(time_res, r_index, lab, pos_frames, status_frames):
    global s1, e1, del_list, del_list1
    # for i in range(0,len(time_res)):
    if s1 < e1:
        ind = r_index.index(s1 + 1)
        bel = tk.Label(status_frames[ind], text=time_res[s1], bg="white", fg="black", font=("Verdana", 40, "bold"))
        bel1 = tk.Label(pos_frames[ind], text=s1 + 1, bg="white", fg="black", font=("Verdana", 40, "bold"))
        bel.pack()
        bel1.pack()
        del_list.append(bel)
        del_list1.append(bel1)
        s1 += 1
        lab.after(500, lambda: text_toDisplay(time_res, r_index, lab, pos_frames, status_frames))


def time_disp(n):
    # print "Pada time_disp "+str(n)
    total = 0
    tot_list = []
    range_plus = [0.41, 0.31, 0.87, 1.23, 0.3, 0.65, 1.01]

    for i in range(0, 8):
        if i == 0:
            total += n
        else:
            total += range_plus[i - 1]
        tot_list.append(round(total, 2))

    random_index = ran_index()
    # print tot_list
    return tot_list, random_index


def Count(lab, pos_frames, status_frames):
    global counter, minutes, time_string, s, s1, del_list, del_list1
    # print "Counter : "+str(counter)
    if counter > 23.43 and counter < 23.44 and time_string == "" or counter > 50.21 and counter < 50.22 and time_string == "":
        # print counter
        time_res, r_index = time_disp(counter)
        s1 = 0
        text_toDisplay(time_res, r_index, lab, pos_frames, status_frames)
        s = 0
        lab.after(6000, lambda: destroy_label(lab))
    if counter >= 60:
        # print "masuk if"
        minutes += 1
        counter = 0.0
        x = str(minutes) + ":" + str(counter)
        time_string = str(minutes) + ":"
    else:
        # print "masuk else"
        x = str(counter)
    # print x
    lab['text'] = time_string + str(x)
    counter += 0.01
    lab.after(10, lambda: Count(lab, pos_frames, status_frames))


def get_events():
    name_ = []
    db = sqlite3.connect('local.db')
    cur = db.cursor()
    query = "SELECT * from EventList LIMIT 45"

    cur.execute(query)

    rows = cur.fetchall()

    for row in rows:
        row = list(row)
        name_.append(str(row[0]) + ":" + str(row[1]) + ":" + str(row[5]))

    db.close()
    return name_


def get_pesertas(id_event, type_event, heat, name_frames, team_frames, lane_frames,event_control_frame):
    global pesertas, teams, heat_, nameeee
    global name_labels_frames,c_panel_names
    global total_competitor
    global laps
    global competitor_split_count
    global pesertas_number
    global prev_time_catch
    global lane_labels
    name_ = []
    c_panel_names_index=[]
    db = sqlite3.connect('local.db')
    cur = db.cursor()
    query_last = "select * from StartList where even_number=" + str(id_event) + " and even_type='" + str(
        type_event) + "' and even_heat=" + str(int(heat) - 1)
    query = "select * from StartList where competitor_status!='X' and even_number=" + str(
        id_event) + " and even_type='" + str(
        type_event) + "' and even_heat=" + str(int(heat))
    query_next = "select * from StartList where competitor_status!='X' and even_number=" + str(
        id_event) + " and even_type='" + str(
        type_event) + "' and even_heat=" + str(int(heat) + 1)

    heat_n = "select even_length from EventList where even_number=" + str(id_event)
    print query

    cur.execute(query)
    rows = cur.fetchall()

    cur.execute(query_next)
    rows_next = cur.fetchall()

    cur.execute(heat_n)
    heat_ = cur.fetchone()
    laps2 = heat_[0]
    laps = laps2 / 50
    for row in rows:
        name_.append(list(row))

    total_competitor = len(rows)

    name_next_ = []
    for row in rows_next:
        name_next_.append(list(row))

    db.close()
    pesertas = name_
    # generate_current_result(pesertas)
    # generate_split(pesertas)
    # generate_running_time()
    # generate_current_heat(pesertas)
    # generate_next_heat(name_next_)
    pesertas_number = len(pesertas)

    for element in name_labels_frames:
        element.destroy()
    for element in teams:
        element.destroy()
    name_labels_frames = []
    teams = []
    print len(name_labels_frames)
    for element in pesertas:
        ind = int(element[4]) - 1
        competitor_split_count[ind] = 0
        name = str(element[5]) + ", " + str(element[6])
        name_labels_frames.append(
            tk.Label(name_frames[ind], text=name, fg="black", bg="yellow", font=("Verdana", 25, "bold")))
        nameeee[ind] = tk.Label(name_frames[ind], text=name, fg="black", bg="yellow", font=("Verdana", 25, "bold"))
        teams.append(
            tk.Label(team_frames[ind], text=element[7], fg="#0009ba", bg="white", font=("Verdana", 20, "bold")))
        teamnamee[ind] = tk.Label(team_frames[ind], text=element[7], fg="#0009ba", bg="white",
                                  font=("Verdana", 20, "bold"))
        c_panel_names.append(tk.Label(event_control_frame, text=name))
        c_panel_names_index.append(ind)
    print c_panel_names_index
    for i in range(0,len(c_panel_names)):
        c_panel_names[i].grid(row=7+c_panel_names_index[i],column=1)
    lane_labels = []
    for i in range(0, len(pesertas)):
        temp = pesertas[i]
        lane = tk.Label(lane_frames[int(temp[4]) - 1], text="L" + str(temp[4]), bg="white", fg="black",
                        font=("Verdana", 25, "bold"))
        lane_labels.append(lane)
        # prev_time_catch[int(temp[4])-1] = None
        prev_time_catch[i] = ""
    for element in lane_labels:
        element.pack()

    print len(name_labels_frames)
    for i in range(0, len(name_labels_frames)):
        name_labels_frames[i].pack(side=tk.LEFT)
    for element in teams:
        element.pack(side=tk.LEFT)


def clear_window(frames):
    # print len(frames)
    # for element in frames:
    # 	print element
    # 	print len(element)
    for element in frames:
        if not element:
            continue
        else:
            try:
                for frame in element:
                    temp_child = frame.winfo_children()
                    try:
                        for child in temp_child:
                            child.destroy()
                    except:
                        print temp_child
                        temp_child.destroy()
            except:
                temp = element.winfo_children()
                for sub_frame in temp:
                    sub_frame.destroy()

def refresh_cpanel_data(ev_frame):
    global queryyy,heatt,c_panel_names
    db = sqlite3.connect('local.db')
    cur = db.cursor()
    query = "select * from StartList where competitor_status!='X' and even_number=" + str(
        queryyy[0]) + " and even_type='" + str(
        queryyy[2]) + "' and even_heat=" + str(heatt)
    cur.execute(query)
    name_=[]
    rows = cur.fetchall()
    for row in rows:
        name_.append(list(row))
    db.close()
    for element in c_panel_names:
        element.destroy()
    c_panel_names=[]
    index_cpanel_names=[]
    for element in name_:
        ind = int(element[4]) - 1
        cpanel_comp_name = str(element[5]) + ", " + str(element[6])
        index_cpanel_names.append(ind)
        c_panel_names.append(tk.Label(ev_frame, text=cpanel_comp_name))
    for i in range(0,len(c_panel_names)):
        c_panel_names[i].grid(row=7+index_cpanel_names[i],column=1)

def action_confirmation(ev_frame,master_wid,comp_id):
    def cancel(widget):
        widget.destroy()

    def data_deletion(master,conf,id):
        global queryyy,heatt
        db = sqlite3.connect('local.db')
        cur = db.cursor()
        query="DELETE from StartList where even_number="+str(queryyy[0])+" and even_heat="+str(heatt)+" and competitor_id="+str(id)
        print query
        cur.execute(query)
        db.commit()
        db.close()
        refresh_cpanel_data(ev_frame)
        conf.destroy()
        master_wid.destroy()

    conf=tk.Toplevel()
    conf.geometry("200x100")
    conf.title("Delete data")
    tk.Label(conf).pack(side=tk.TOP)
    tk.Label(conf,text="You are about to delete this data.\nAre you sure?").pack(side=tk.TOP)
    tk.Button(conf,text="Delete",command=lambda:data_deletion(master_wid,conf,comp_id)).pack()
    tk.Button(conf,text="Cancel",command=lambda:cancel(conf)).pack()

def change_detailData(ev_frame,number,heat,comp_id,firstname,lastname,lane,cflag,lane_now,data_flag,main_frame):
    global queryyy,heatt,c_panel_names
    event_number=number.get()
    event_heat=heat.get()
    comp_first=firstname.get()
    comp_last=lastname.get()
    comp_lane=lane.get()
    comp_flag=cflag.get()
    print event_number,event_heat,comp_first,comp_last,comp_lane,comp_flag

    if data_flag==True:
        print "If"
        db = sqlite3.connect('local.db')
        cur = db.cursor()
        query="UPDATE StartList SET even_number="+str(event_number)+",even_heat="+str(event_heat)+",competitor_lane="+str(comp_lane)+',competitor_firstname="'+str(comp_first)+'",competitor_lastname="'+str(comp_last)+'",competitor_flag="'+str(comp_flag)+'" where even_number='+str(queryyy[0])+" and even_heat="+str(heatt)+" and competitor_id="+str(comp_id)
        print query
        cur.execute(query)
        db.commit()
        db.close()

        db = sqlite3.connect('local.db')
        cur = db.cursor()
        cur.execute('SELECT * FROM StartList WHERE even_number="' + str(queryyy[0]) + '"'+" and even_heat="+str(heatt)+" and competitor_lane="+str(lane_now))
        row_record = cur.fetchone()
        e_number,e_type,e_heat,c_id,c_lane,first_name,last_name,fl,te,stat,c_time=row_record
        db.close()

        number.delete(0,tk.END)
        number.insert(tk.END,e_number)
        heat.delete(0,tk.END)
        heat.insert(tk.END,e_heat)
        firstname.delete(0,tk.END)
        firstname.insert(tk.END,first_name)
        lastname.delete(0,tk.END)
        lastname.insert(tk.END,last_name)
        lane.delete(0,tk.END)
        lane.insert(tk.END,c_lane)
        cflag.delete(0,tk.END)
        cflag.insert(tk.END,fl)
    else:
        print "Else"
        db = sqlite3.connect('local.db')
        cur = db.cursor()
        query='INSERT INTO StartList values ('+str(event_number)+',"'+str(queryyy[2])+'",'+str(event_heat)+','+str(randint(900,1000))+','+str(comp_lane)+',"'+str(comp_first)+'","'+str(comp_last)+'","'+str(comp_flag)+'",'+"NULL"+',"'+""+'",NULL'+')'
        print query
        cur.execute(query)
        db.commit()
        db.close()
        refresh_cpanel_data(ev_frame)
        main_frame.destroy()
    refresh_cpanel_data(ev_frame)

def change_detailWindow(ev_frame,lane):
    global heatt,queryyy
    print lane
    data_exist=False

    db = sqlite3.connect('local.db')
    cur = db.cursor()
    cur.execute('SELECT * FROM StartList WHERE even_number="' + str(queryyy[0]) + '"'+" and even_heat="+str(heatt)+" and competitor_lane="+str(lane))
    row_record = cur.fetchone()
    print row_record
    try:
        event_number,event_type,event_heat,comp_id,comp_lane,first_name,last_name,flag,team,status,comp_time=row_record
        data_exist=True
    except:
        event_number=queryyy[0]
        event_type=queryyy[2]
        event_heat=heatt
        comp_id=""
        comp_lane=lane
        first_name=""
        last_name=""
        flag=""
        team=""
        status=""
        comp_time=""
    comp_name=str(first_name)+" "+str(last_name)
    db.close()
    details=tk.Toplevel()
    details.title("Edit "+comp_name+" data")
    details.geometry("400x200")
    details_delete=tk.Button(details,text="Delete Data",command=lambda:action_confirmation(ev_frame,details,comp_id))
    details_delete.grid(row=0,column=3)

    tk.Label(details,text="Event ID Number").grid(row=0,column=0)
    ev_number=tk.Entry(details)
    ev_number.insert(tk.END,event_number)
    ev_number.grid(row=0,column=1)

    tk.Label(details,text="Event Heat").grid(row=1,column=0)
    ev_heat=tk.Entry(details)
    ev_heat.insert(tk.END,event_heat)
    ev_heat.grid(row=1,column=1)

    tk.Label(details,text="First Name").grid(row=2,column=0)
    ev_firstName=tk.Entry(details)
    ev_firstName.insert(tk.END,first_name)
    ev_firstName.grid(row=2,column=1)

    tk.Label(details,text="Last Name").grid(row=3,column=0)
    ev_lastName=tk.Entry(details)
    ev_lastName.insert(tk.END,last_name)
    ev_lastName.grid(row=3,column=1)

    tk.Label(details,text="Lane").grid(row=4,column=0)
    ev_lane=tk.Entry(details)
    ev_lane.insert(tk.END,comp_lane)
    ev_lane.grid(row=4,column=1)

    tk.Label(details,text="Flag").grid(row=5,column=0)
    ev_team=tk.Entry(details)
    ev_team.insert(tk.END,flag)
    ev_team.grid(row=5,column=1)

    tk.Label(details).grid(row=6)
    tk.Button(details,text="Change",command=lambda:change_detailData(ev_frame,ev_number,ev_heat,comp_id,ev_firstName,ev_lastName,ev_lane,ev_team,lane,data_exist,details)).grid(row=7,column=1)

def getText(var, title, mode_label, nr_label, mr_label):
    global queryyy, row_record
    global mode_lomba, recorded_time, record_flag
    queryyy = var.get()
    queryyy = queryyy.split(":")

    db = sqlite3.connect('local.db')
    cur = db.cursor()
    cur.execute('SELECT even_nr,even_mr FROM EventList WHERE even_number="' + str(queryyy[0]) + '"')
    row_record = cur.fetchone()
    db.close()

    for i in range(0, 15):
        recorded_time.append(str(""))

    title['text'] = queryyy[0] + " - " + queryyy[1]
    nr_label['text'] = "NR: " + str(row_record[0])
    mr_label['text'] = "MR: " + str(row_record[1])
    if queryyy[2] == "F":
        mode_lomba = "Final"
    elif queryyy[2] == "P":
        mode_lomba = "Preliminary"
    mode_label['text'] = mode_lomba
    record_flag = None


def getHeat(var, mode_label, framess):
    global heatt, mode_lomba, counter_pesertas
    global ranked_names, ranked_teams, ranked_times, ranked_lanes, c_panel_names
    global title_official
    global dq_del_cp
    ranked_names = []
    ranked_teams = []
    ranked_times = []
    ranked_lanes = []
    counter_pesertas = 0
    title_official['text'] = ""
    clear_window(framess)
    for element in c_panel_names:
        element.destroy()
    for element in dq_del_cp:
        element.destroy()
    c_panel_names=[]
    dq_del_cp=[]
    heatt = var.get()
    mode_label['text'] = str(mode_lomba) + " " + str(heatt)


def update_waktu():
    global timer_now
    print "Masuk Process"
    while True:
        waktu = str(datetime.datetime.time(datetime.datetime.now()))
        try:
            waktu, sisa = waktu.split('.')
            timer_now['text'] = waktu
        except:
            timer_now['text'] = waktu
        sleep(1)


# def update_waktu():
#     global timer_now
#     print "Masuk Process"
#     waktu = str(datetime.datetime.time(datetime.datetime.now()))
#     try:
#         waktu, sisa = waktu.split('.')
#         timer_now['text'] = waktu
#         timer_now.after(1000,lambda:update_waktu())
#     except:
#         timer_now['text'] = waktu
#         timer_now.after(1000,lambda:update_waktu())

def get_medal(chosed_event, medal_window):
    global app, colors

    temp_event = str(chosed_event.get())
    event_num, event_name, event_type = temp_event.split(":")
    db = sqlite3.connect('local.db')
    cur = db.cursor()

    final_query = 'SELECT * FROM StartList WHERE competitor_time!="" AND even_number=' + event_num + ' ORDER BY competitor_time LIMIT 3'
    cur.execute(final_query)
    rows = cur.fetchall()
    print rows
    db.close()
    result_rows = []
    for row in rows:
        row = list(row)
        result_rows.append(row)
    len_res = len(result_rows)
    def_w = 1366
    batas_lane = 10
    h_comp = 75
    competitor_frames = []
    lane_frames = []
    team_frames = []
    logo_frames = []
    name_frames = []
    pos_frames = []
    status_frames = []
    ###defines - Stop

    # Sanitasi finalFrame_contents - Start
    clear_window([medal_window])
    # Sanitasi finalFrame_contents - Stop

    # ###Layout Design - Start
    # Frame Title - Start
    frame_title = app.frames["MedalDisplay"].frame_title_medal
    # Frame Title - Stop

    # Frame Bawah Title - Start
    frame_something = app.frames["MedalDisplay"].frame_official_medal
    # # Frame Bawah Title - Stop

    # --Dorong sedikit ke tengah
    # separator_title = tk.Frame(medal_window,height=150, bd=1, relief=tk.SUNKEN)
    # separator_title.pack()
    # Frame Competitor - Start
    for i in range(0, len_res):
        temp = tk.Frame(medal_window, width=def_w, height=h_comp, bg="gray", bd=1)
        competitor_frames.append(temp)
    for i in range(0, len_res):
        competitor_frames[i].pack()
        separator = tk.Frame(height=10, bd=1, relief=tk.SUNKEN)
        separator.pack()
    # Frame Competitor - Stop

    ###Layout Design - Stop

    ###Competitor Content Frames - Start
    # Frame Lane - Start
    for i in range(0, len_res):
        temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
        lane_frames.append(temp)
    for i in range(0, len_res):
        lane_frames[i].pack_propagate(0)
        lane_frames[i].pack(side=tk.LEFT)
        separator1 = tk.Frame(width=2, height=65, bd=1, relief=tk.SUNKEN)
        separator1.pack(side=tk.LEFT)
    # Frame Lane - Stop

    # Frame Team - Start
    for i in range(0, len_res):
        temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
        temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
        team_frames.append(temp)
        logo_frames.append(temp1)
    for i in range(0, len_res):
        team_frames[i].pack_propagate(0)
        team_frames[i].pack(side=tk.LEFT)
        logo_frames[i].pack_propagate(0)
        logo_frames[i].pack(side=tk.LEFT)
    # Frame Team - Stop

    # Frame CompName - Start
    for i in range(0, len_res):
        temp1 = tk.Frame(competitor_frames[i], width=600, height=h_comp, bg=colors[i])
        name_frames.append(temp1)
    for i in range(0, len_res):
        name_frames[i].pack_propagate(0)
        name_frames[i].pack(side=tk.LEFT)
    # Frame CompName - Stop

    # Frame Status - Start
    for i in range(0, len_res):
        temp1 = tk.Frame(competitor_frames[i], width=364, height=h_comp, bg="white")
        status_frames.append(temp1)
    for i in range(0, len_res):
        status_frames[i].pack_propagate(0)
        status_frames[i].pack(side=tk.LEFT)
        separator2 = tk.Frame(competitor_frames[i], width=2, height=h_comp, bd=1, relief=tk.SUNKEN, bg="black")
        separator2.pack(side=tk.LEFT)
    # Frame Status - Stop

    # Frame Pos - Start
    for i in range(0, len_res):
        temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
        pos_frames.append(temp1)
    for i in range(0, len_res):
        pos_frames[i].pack_propagate(0)
        pos_frames[i].pack(side=tk.LEFT)
    # Frame Pos - Stop

    ###Competitor Content Frames - Stop

    ###Title Content
    image = Image.open("imgs/ipoh_logo_small.png")
    photo = ImageTk.PhotoImage(image)
    logo_label = tk.Label(frame_title, image=photo)
    logo_label.image = photo
    logo_label.pack(side="left")
    title_label = tk.Label(frame_title, text=event_num + " - " + event_name, bg="black", fg="white",
                           font=("Verdana", 30, "bold"))
    title_label.pack()
    mode_label = tk.Label(frame_title, text="MEDAL CEREMONY", bg="black", fg="white", font=("Verdana", 25, "bold"))
    mode_label.pack()
    ##Title Content

    title_something = tk.Label(frame_something, text="", bg="gray", fg="black", font=("Verdana", 20, "bold"))
    title_something.pack()

    for i in range(0, len(team_frames)):
        temp = result_rows[i]
        tk.Label(team_frames[i], text=temp[7], fg="white", bg="black", font=("Verdana", 20, "bold")).pack(side=tk.LEFT)
        tk.Label(name_frames[i], text=temp[5] + ", " + temp[6], fg="white", bg=colors[i],
                 font=("Verdana", 20, "bold")).pack(side=tk.LEFT)
        # if i>7:
        #     tk.Label(pos_frames[i],text="R",bg="white",fg="black",font=("Verdana",40,"bold")).pack()
        # tk.Label(pos_frames[i],text="Q",bg="white",fg="black",font=("Verdana",40,"bold")).pack()
        tk.Label(lane_frames[i], text=str(i + 1), bg="white", fg="black", font=("Verdana", 20, "bold")).pack(
            side=tk.LEFT)
        tk.Label(status_frames[i], text=temp[len(temp) - 1], bg="white", fg="black", font=("Verdana", 20, "bold")).pack(
            side=tk.LEFT)


def reset_final(frame):
    global final_draw_flag
    final_draw_flag = False
    clear_window([frame])


def get_final(chosed_event, finalist_window, en):
    global final_draw_flag, timer_now, looping_start

    def draw(flagg, event_num, event_name, event_type, result_rows):
        global looping_start
        def_w = 1366
        batas_lane = 10
        h_comp = 50
        competitor_frames = []
        lane_frames = []
        team_frames = []
        logo_frames = []
        name_frames = []
        pos_frames = []
        status_frames = []
        ###defines - Stop

        # Sanitasi finalFrame_contents - Start
        clear_window([finalist_window])
        # Sanitasi finalFrame_contents - Stop

        ###Layout Design - Start
        # Frame Title - Start
        frame_title = tk.Frame(finalist_window, width=def_w, height=90, bg="black")
        frame_title.pack_propagate(0)
        frame_title.pack()
        # Frame Title - Stop

        # Frame Bawah Title - Start
        frame_something = tk.Frame(finalist_window, width=def_w, height=40, bg="gray")
        frame_something.pack_propagate(0)
        frame_something.pack()
        # Frame Bawah Title - Stop

        # Frame Competitor - Start
        for i in range(0, batas_lane):
            temp = tk.Frame(finalist_window, width=def_w, height=h_comp, bg="gray", bd=1)
            competitor_frames.append(temp)
        for i in range(0, batas_lane):
            competitor_frames[i].pack()
            separator = tk.Frame(height=2, bd=1, relief=tk.SUNKEN)
            separator.pack()
        # Frame Competitor - Stop

        # Frame Timer - Start
        frame_timer = tk.Frame(finalist_window, width=def_w, height=102, bg="black")
        frame_timer.pack_propagate(0)
        frame_timer.pack()
        # Frame Timer - Stop
        ###Layout Design - Stop

        ###Competitor Content Frames - Start
        # Frame Lane - Start
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            lane_frames.append(temp)
        for i in range(0, batas_lane):
            lane_frames[i].pack_propagate(0)
            lane_frames[i].pack(side=tk.LEFT)
            separator1 = tk.Frame(width=2, height=65, bd=1, relief=tk.SUNKEN)
            separator1.pack(side=tk.LEFT)
        # Frame Lane - Stop

        # Frame Team - Start
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="yellow")
            team_frames.append(temp)
            logo_frames.append(temp1)
        for i in range(0, batas_lane):
            team_frames[i].pack_propagate(0)
            team_frames[i].pack(side=tk.LEFT)
            logo_frames[i].pack_propagate(0)
            logo_frames[i].pack(side=tk.LEFT)
        # Frame Team - Stop

        # Frame CompName - Start
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=600, height=h_comp, bg="yellow")
            name_frames.append(temp1)
        for i in range(0, batas_lane):
            name_frames[i].pack_propagate(0)
            name_frames[i].pack(side=tk.LEFT)
        # Frame CompName - Stop

        # Frame Pos - Start
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
            pos_frames.append(temp1)
        for i in range(0, batas_lane):
            pos_frames[i].pack_propagate(0)
            pos_frames[i].pack(side=tk.LEFT)
            separator2 = tk.Frame(competitor_frames[i], width=2, height=h_comp, bd=1, relief=tk.SUNKEN, bg="black")
            separator2.pack(side=tk.LEFT)
        # Frame Pos - Stop

        # Frame Status - Start
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=364, height=h_comp, bg="black")
            status_frames.append(temp1)
        for i in range(0, batas_lane):
            status_frames[i].pack_propagate(0)
            status_frames[i].pack(side=tk.LEFT)
        # Frame Status - Stop
        ###Competitor Content Frames - Stop

        # db = sqlite3.connect('local.db')
        # cur = db.cursor()
        # cur.execute('SELECT even_nr,even_mr FROM EventList WHERE even_number="' + str(queryyy[0]) + '"')
        # row_record = cur.fetchone()
        # db.close()

        ###Title Content
        image = Image.open("imgs/ipoh_logo_small.png")
        photo = ImageTk.PhotoImage(image)
        logo_label = tk.Label(frame_title, image=photo)
        logo_label.image = photo
        logo_label.pack(side="left")

        mrnr_frame = tk.Frame(frame_title, bg="black")
        mrnr_frame.pack(side="right", fill=tk.Y)

        title_label = tk.Label(frame_title, text=event_num + " - " + event_name, bg="black", fg="white",
                               font=("Verdana", 30, "bold"))
        title_label.pack()

        mode_label = tk.Label(frame_title, text="Final Result", bg="black", fg="white", font=("Verdana", 25, "bold"))
        mode_label.pack()

        nr_label = tk.Label(mrnr_frame, text="NR: " + str(row_record[0]), bg="black", fg="white",
                            font=("Verdana", 25, "bold"))
        nr_label.grid(row=0, column=0, sticky="we")

        mr_label = tk.Label(mrnr_frame, text="MR: " + str(row_record[1]), bg="black", fg="white",
                            font=("Verdana", 25, "bold"))
        mr_label.grid(row=1, column=0, sticky="we")

        # ghost_label = tk.Label(mrnr_frame, text="pad", bg="black", fg="black", font=("Verdana", 30, "bold"))
        # ghost_label.grid(row=1, column=1, columnspan=1)
        ###Title Content

        title_something = tk.Label(frame_something, text="", bg="gray", fg="black", font=("Verdana", 15, "bold"))
        title_something.pack()
        ###Timer - Start
        sep = tk.Frame(frame_timer, width=20, height=65, relief=tk.SUNKEN, bg="white")
        sep.pack(side=tk.RIGHT)

        # waktu_skrg = str(datetime.datetime.time(datetime.datetime.now()))
        # try:
        #     waktu_skrg, sisa = waktu_skrg.split('.')
        #     print waktu_skrg
        # except:
        #     print "masuk except"
        #     print waktu_skrg
        #
        # timer_now=tk.Label(frame_timer,text=waktu_skrg,fg="black",bg="white",font=("Verdana",35,"bold"))
        # timer_now.pack(side=tk.LEFT)

        for i in range(0, batas_lane):
            try:
                temp = result_rows[i]
            except:
                tk.Label(lane_frames[i], text=str(looping_start + 1), bg="white", fg="black", font=("Verdana", 25, "bold")).pack(
                    side=tk.LEFT)
                looping_start+=1
                continue
            print temp
            print temp[len(temp) - 2], type(temp[len(temp) - 2])
            tk.Label(team_frames[i], text=temp[7], fg="#0009ba", bg="white", font=("Verdana", 25, "bold")).pack(
                side=tk.LEFT)
            tk.Label(name_frames[i], text=temp[5] + ", " + temp[6], fg="black", bg="yellow",
                     font=("Verdana", 20, "bold")).pack(side=tk.LEFT)
            # if i>7:
            #     tk.Label(pos_frames[i],text="R",bg="white",fg="black",font=("Verdana",40,"bold")).pack()
            # tk.Label(pos_frames[i],text="Q",bg="white",fg="black",font=("Verdana",40,"bold")).pack()
            tk.Label(lane_frames[i], text=str(looping_start + 1), bg="white", fg="black", font=("Verdana", 25, "bold")).pack(
                side=tk.LEFT)
            looping_start += 1

            if temp[len(temp) - 2] == "DQ":
                time = "DQ"
            elif temp[len(temp) - 2] == "DNS":
                time = "DNS"
            else:
                time = str(temp[len(temp) - 1])

            tk.Label(status_frames[i], text=time, bg="black", fg="white", font=("Verdana", 25, "bold")).pack(
                side=tk.LEFT)

    def do(event_num, event_name, event_type, n, x, data):
        global looping_start
        if final_draw_flag == False:
            return
        draw(True, event_num, event_name, event_type, data[x])
        if x + 1 >= len(data):
            x = 0
            looping_start = 0
        else:
            x += 1
        finalist_window.after(n * 1000, lambda: do(event_num, event_name, event_type, n, x, data))

    temp_event = str(chosed_event.get())
    event_num, event_name, event_type = temp_event.split(":")

    db = sqlite3.connect('local.db')
    cur = db.cursor()

    cur.execute('SELECT even_nr,even_mr FROM EventList WHERE even_number="' + str(event_num) + '"')
    row_record = cur.fetchone()

    final_query = 'SELECT * FROM StartList WHERE competitor_time!="" AND even_number=' + event_num + ' ORDER BY competitor_time'
    cur.execute(final_query)
    rows = cur.fetchall()
    # print rows
    result_rows = []
    for row in rows:
        row = list(row)
        result_rows.append(row)

    empty_query = 'SELECT * FROM StartList WHERE competitor_time="" AND even_number=' + event_num + ' ORDER BY even_heat'
    cur.execute(empty_query)
    rows_empty = cur.fetchall()
    if rows_empty:
        for row in rows_empty:
            row = list(row)
            result_rows.append(row)

    disq_query = 'SELECT * FROM StartList WHERE competitor_status="DQ" AND even_number=' + event_num + ' ORDER BY even_heat'
    cur.execute(disq_query)
    rows_disq = cur.fetchall()
    if rows_disq:
        for row in rows_disq:
            row = list(row)
            result_rows.append(row)

    dns_query = 'SELECT * FROM StartList WHERE competitor_status="DNS" AND even_number=' + event_num + ' ORDER BY even_heat'
    cur.execute(dns_query)
    rows_dns = cur.fetchall()
    if rows_dns:
        for row in rows_dns:
            row = list(row)
            result_rows.append(row)

    batas_loop = len(result_rows)
    looping_start = 0
    en = en.get()
    try:
        loop_sleep = int(en)
        print "masuk try"
        per10 = [result_rows[x:x + 10] for x in xrange(0, len(result_rows), 10)]
        start_loop = 0
        final_draw_flag = True
        do(event_num, event_name, event_type, loop_sleep, start_loop, per10)
    except:
        print "masuk except"
        draw(False, event_num, event_name, event_type, result_rows)


class App(tk.Tk):

    def __init__(self, *args, **kwargs):
        global app_container, final_display_frame
        tk.Tk.__init__(self, *args, **kwargs)
        self.title("DisMan-Proto")
        self.geometry("1366x768")

        self.title_font = tkfont.Font(family='Verdana', size=18, weight="bold", slant="italic")

        app_container = tk.Frame(self, bg="black")
        app_container.pack(side="top", fill="both", expand=True)
        app_container.grid_rowconfigure(0, weight=1)
        app_container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        self.frames["Display"] = Display(parent=app_container, controller=self)
        final_display_frame = FinalAnnounceDisplay(parent=app_container, controller=self)
        self.frames["MedalDisplay"] = MedalDisplay(parent=app_container, controller=self)
        self.frames["ImageDisplay"] = ImageDisplay(parent=app_container, controller=self)
        self.frames["AnnounceDisplay"] = AnnounceDisplay(parent=app_container, controller=self)

        self.frames["Display"].grid(row=0, column=0, sticky="nsew")
        final_display_frame.grid(row=0, column=0, sticky="nsew")
        self.frames["MedalDisplay"].grid(row=0, column=0, sticky="nsew")
        self.frames["ImageDisplay"].grid(row=0, column=0, sticky="nsew")
        self.frames["AnnounceDisplay"].grid(row=0, column=0, sticky="nsew")

        self.show_frame("Display")

    def show_frame(self, page_name):
        if page_name == "FinalDisplay":
            frame = final_display_frame
            frame.tkraise()
        elif page_name == "MedalImage":
            self.frames["MedalDisplay"].show_medal_frame("MedalImage")
        elif page_name == "MedalResult":
            self.frames["MedalDisplay"].show_medal_frame("MedalResult")
        else:
            frame = self.frames[page_name]
            frame.tkraise()


class Display(tk.Frame):

    def __init__(self, parent, controller):
        global def_w, h_comp, batas_lane
        global title_official, final_display_frame, container_medal_result
        global timer_now

        tk.Frame.__init__(self, parent, bg="black")
        self.controller = controller

        # Frame Title
        frame_title = tk.Frame(self, width=def_w, height=90, bg="black")
        frame_title.pack_propagate(0)
        frame_title.pack()

        frame_official = tk.Frame(self, width=def_w, height=40, bg="black")
        frame_official.pack_propagate(0)
        frame_official.pack()

        # Frame Competitor
        competitor_frames = []
        for i in range(0, batas_lane):
            temp = tk.Frame(self, width=def_w, height=h_comp, bg="gray", bd=1)
            competitor_frames.append(temp)
        for i in range(0, batas_lane):
            competitor_frames[i].pack()
            separator = tk.Frame(self, height=2, bd=1, bg="black", relief=tk.SUNKEN)
            separator.pack()

        # Frame Timer
        frame_timer = tk.Frame(self, width=def_w, height=102, bg="black")
        frame_timer.pack_propagate(0)
        frame_timer.pack()

        ###Competitor Content Frames - Start
        lane_frames = []
        # Frame Lane - Start
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            lane_frames.append(temp)
        for i in range(0, batas_lane):
            lane_frames[i].pack_propagate(0)
            lane_frames[i].pack(side=tk.LEFT)
            separator1 = tk.Frame(width=2, height=65, bd=1, bg="black", relief=tk.SUNKEN)
            separator1.pack(side=tk.LEFT)
        # Frame Lane - Stop

        # Frame Team - Start
        team_frames = []
        logo_frames = []
        for i in range(0, batas_lane):
            temp = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="white")
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="yellow")
            team_frames.append(temp)
            logo_frames.append(temp1)
        for i in range(0, batas_lane):
            team_frames[i].pack_propagate(0)
            team_frames[i].pack(side=tk.LEFT)
            logo_frames[i].pack_propagate(0)
            logo_frames[i].pack(side=tk.LEFT)
        # Frame Team - Stop

        # Frame CompName - Start
        name_frames = []
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=600, height=h_comp, bg="yellow")
            name_frames.append(temp1)
        for i in range(0, batas_lane):
            name_frames[i].pack_propagate(0)
            name_frames[i].pack(side=tk.LEFT)
        # Frame CompName - Stop

        # Frame Pos - Start
        pos_frames = []
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=100, height=h_comp, bg="black")
            pos_frames.append(temp1)
        for i in range(0, batas_lane):
            pos_frames[i].pack_propagate(0)
            pos_frames[i].pack(side=tk.LEFT)
            separator2 = tk.Frame(competitor_frames[i], width=2, height=h_comp, bd=1, relief=tk.SUNKEN, bg="white")
            separator2.pack(side=tk.LEFT)
        # Frame Pos - Stop

        # Frame Status - Start
        status_frames = []
        for i in range(0, batas_lane):
            temp1 = tk.Frame(competitor_frames[i], width=364, height=h_comp, bg="black")
            status_frames.append(temp1)
        for i in range(0, batas_lane):
            status_frames[i].pack_propagate(0)
            status_frames[i].pack(side=tk.LEFT)
        # Frame Status - Stop
        ###Competitor Content Frames - Stop

        ###Title Content
        image = Image.open("imgs/ipoh_logo_small.png")
        photo = ImageTk.PhotoImage(image)
        logo_label = tk.Label(frame_title, image=photo)
        logo_label.image = photo
        logo_label.pack(side="left")

        mrnr_frame = tk.Frame(frame_title, bg="black")
        mrnr_frame.pack(side="right", fill=tk.Y)

        title_label = tk.Label(frame_title, text="Perak Invitational Swimming Championship", bg="black", fg="yellow",
                               font=("Verdana", 30, "bold"))
        title_label.pack()

        mode_label = tk.Label(frame_title, text="", bg="black", fg="white", font=("Verdana", 30, "bold"))
        mode_label.pack()

        nr_label = tk.Label(mrnr_frame, text="", bg="black", fg="white", font=("Verdana", 23, "bold"))
        nr_label.grid(row=0, column=0, sticky="wes")

        mr_label = tk.Label(mrnr_frame, text="", bg="black", fg="white", font=("Verdana", 23, "bold"))
        mr_label.grid(row=1, column=0, sticky="wen")

        # ghost_label = tk.Label(mrnr_frame, text="pad", bg="black", fg="black", font=("Verdana", 30, "bold"))
        # ghost_label.grid(row=1, column=1, columnspan=1)

        title_official = tk.Label(frame_official, text="", bg="black", fg="white", font=("Verdana", 20, "bold"))
        title_official.pack()
        ###Title Content

        ## Timer
        sep = tk.Frame(frame_timer, width=20, height=65, relief=tk.SUNKEN, bg="white")
        sep.pack(side=tk.RIGHT)
        waktu_skrg = str(datetime.datetime.time(datetime.datetime.now()))
        try:
            waktu_skrg, sisa = waktu_skrg.split('.')
            print waktu_skrg
        except:
            print "masuk except"
            print waktu_skrg
        timer_now = tk.Label(frame_timer, text=waktu_skrg, fg="white", bg="black", font=("Verdana", 35, "bold"))
        timer_now.pack(side=tk.LEFT)

        timer_label = tk.Label(frame_timer, text=0.0, fg="white", bg="black", font=("Verdana", 40, "bold"))
        timer_label.pack(side=tk.RIGHT)
        ## Timer - Stop

        ###Lane Numbers - Start
        # lane_labels = []
        # for i in range(0, batas_lane):
        #     lane = tk.Label(lane_frames[i], text="L" + str(i + 1), bg="white", fg="black", font=("Verdana", 40, "bold"))
        #     lane_labels.append(lane)
        # for element in lane_labels:
        #     element.pack()
        ###Lane Numbers - Stop

        th = Thread(target=lambda: listener(self, timer_label, status_frames, pos_frames, name_frames))
        th.start()
        th_w = Thread(target=update_waktu)
        th_w.start()

        #####
        controlPanel = tk.Toplevel()
        controlPanel.title("Control Panel")
        controlPanel.geometry("1000x500")

        rows = 0
        while rows < 30:
            controlPanel.rowconfigure(rows, weight=1)
            controlPanel.columnconfigure(rows, weight=1)
            rows += 1

        nb = ttk.Notebook(controlPanel)
        nb.grid(row=0, column=0, columnspan=30, rowspan=30, sticky='nesw')

        event_control = ttk.Frame(nb)
        nb.add(event_control, text='Event')

        final_announcement_control = ttk.Frame(nb)
        nb.add(final_announcement_control, text='Final Event Announcement')

        medal_control = ttk.Frame(nb)
        nb.add(medal_control, text='Medals')

        image_control = ttk.Frame(nb)
        nb.add(image_control, text='Graphics')

        announcement_control = ttk.Frame(nb)
        nb.add(announcement_control, text='Announcement')
        #####

        # Event Control Panel
        tk.Label(event_control,text="EVENT").grid(row=0,column=0)
        var = tk.StringVar(event_control)
        var.set("Choose One")  # initial value
        options = get_events()
        option = apply(tk.OptionMenu, (event_control, var) + tuple(options))
        option.grid(row=1, column=0)

        buttonEvent = tk.Button(event_control, text="Choose Events",
                                command=lambda: getText(var, title_label, mode_label, nr_label, mr_label))
        buttonEvent.grid(row=1, column=1)

        tk.Label(event_control,text="HEAT").grid(row=0,column=3)

        but_event_display = tk.Button(event_control, text="- Change View to Event -",
                                      command=lambda: controller.show_frame("Display"))
        but_event_display.grid(row=0, column=5)

        var1 = tk.StringVar(event_control)
        var1.set("Choose One")  # initial value
        options1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        option1 = apply(tk.OptionMenu, (event_control, var1) + tuple(options1))
        option1.grid(row=1, column=3)

        butHeat = tk.Button(event_control, text="Choose Heat", command=lambda: getHeat(var1, mode_label,
                                                                                       [lane_frames, team_frames,
                                                                                        logo_frames,
                                                                                        name_frames, pos_frames,
                                                                                        status_frames]))
        butHeat.grid(row=1, column=4)

        butPes = tk.Button(event_control, text="* Show *",
                           command=lambda: get_pesertas(queryyy[0], queryyy[2], heatt, name_frames, team_frames,
                                                        lane_frames,event_control))
        butPes.grid(row=1, column=5)
        butResultAkhir = tk.Button(event_control, text="* Get Result *",
                                   command=lambda: replace_ranked(name_frames,status_frames,team_frames, lane_frames, pos_frames))
        butResultAkhir.grid(row=2, column=0)

        butOfficiel = tk.Button(event_control, text="Change Unofficial",
                                command=lambda: self.changeOfficial("UNOFFICIAL"))
        butOfficiel.grid(row=4, column=0)

        butOfficiel2 = tk.Button(event_control, text="Change Official", command=lambda: self.changeOfficial("OFFICIAL"))
        butOfficiel2.grid(row=4, column=1)

        default_entry = tk.Entry(event_control)
        default_entry.grid(row=3, column=2)

        event_entry = tk.Entry(event_control)
        event_entry.grid(row=4, column=2)
        event_entry_but = tk.Button(event_control, text="Change Official Text",
                                    command=lambda: self.changeOfficialText(event_entry, default_entry))
        event_entry_but.grid(row=4, column=3)
        tk.Label(event_control).grid(row=5)
        # buttons
        tk.Label(event_control,text="LANE").grid(row=6,column=0)
        for i in range(0,8):
            tk.Label(event_control,text=i+1).grid(row=7+i,column=0)
        tk.Label(event_control,text="Competitors").grid(row=6,column=1)
        tk.Label(event_control,text="DQ").grid(row=6,column=3)
        tk.Label(event_control,text="DQ Status").grid(row=6,column=4)
        tk.Label(event_control,text="DNS").grid(row=6,column=5)
        tk.Label(event_control,text="DNS Status").grid(row=6,column=6)
        teks = []
        for i in range(0, batas_lane):
            teks.append(i)

        for i in range(0, 8):
            butDQ = tk.Button(event_control, text="L" + str(teks[i] + 1) + "-DQ",
                              command=lambda iiDQ=i: disq(status_frames, teks[iiDQ],event_control,3))
            butDQ.grid(column=3, row=int(7 + i))

        for i in range(0, 8):
            butDNS = tk.Button(event_control, text="L" + str(teks[i] + 1) + "-DNS",
                               command=lambda ii=i: dns(status_frames, teks[ii],event_control,5))
            butDNS.grid(column=5, row=int(7 + i))

        for i in range(0, 8):
            butDNS = tk.Button(event_control, text="Comp Details",
                               command=lambda iiChd=i: change_detailWindow(event_control,iiChd+1))
            butDNS.grid(column=2, row=int(7 + i))
    
        tk.Button(event_control, text="Quit", command=self.quitApp).grid(row=0,column=6)
        ############################

        but_final_display = tk.Button(final_announcement_control, text="- Change view to Results -",
                                      command=lambda: controller.show_frame("FinalDisplay"))
        but_final_display.grid(row=1)

        duration_entry = tk.Entry(final_announcement_control)
        duration_entry.grid(row=2)

        var2 = tk.StringVar(event_control)
        var2.set("Choose One")
        options1 = get_events()
        option1 = apply(tk.OptionMenu, (final_announcement_control, var2) + tuple(options1))
        option1.grid(row=3, column=0)

        but_final_show = tk.Button(final_announcement_control, text="Show",
                                   command=lambda: get_final(var2, final_display_frame, duration_entry))
        but_final_show.grid(row=3, column=1)
        tk.Button(final_announcement_control, text="Reset", command=lambda: reset_final(final_display_frame)).grid(
            row=3, column=2)

        ############################

        but_medal_display = tk.Button(medal_control, text="- Change view to Medal -",
                                      command=lambda: controller.show_frame("MedalDisplay"))
        but_medal_display.grid(row=0, column=0)

        but_medal_display_image = tk.Button(medal_control, text="Change to Medal Image",
                                            command=lambda: controller.show_frame("MedalImage"))
        but_medal_display_image.grid(row=0, column=1)

        but_medal_display_result = tk.Button(medal_control, text="Change to Medal Result",
                                             command=lambda: controller.show_frame("MedalResult"))
        but_medal_display_result.grid(row=1, column=1)

        var3 = tk.StringVar(medal_control)
        var3.set("Choose One")
        options3 = get_events()
        option3 = apply(tk.OptionMenu, (medal_control, var3) + tuple(options3))
        option3.grid(row=2, column=0)

        but_medal_result_init = tk.Button(medal_control, text="* Set Medal Event *",
                                          command=lambda: get_medal(var3, container_medal_result))
        but_medal_result_init.grid(row=2, column=1)

        ############################

        img_dir = "imgs/"
        img_col_pos = 0
        img_row_pos = 0

        for names in os.listdir(img_dir):
            img_path_1 = img_dir + names
            temp_img_1 = Image.open(img_path_1)
            temp_img_1 = temp_img_1.resize((100, 75))
            img_photo_1 = ImageTk.PhotoImage(temp_img_1)
            img_prev_1 = tk.Label(image_control, image=img_photo_1)
            img_prev_1.image = img_photo_1
            img_prev_1.grid(row=img_row_pos, column=img_col_pos)

            but_image_display = tk.Button(image_control, text="Change view to this Image",
                                          command=lambda i=img_path_1: self.update_display_image(i))
            but_image_display.grid(row=img_row_pos + 2, column=img_col_pos)

            img_col_pos += 2

            if img_col_pos >= 10:
                img_col_pos = 0
                img_row_pos += 6

        ############################

        but_announce_display = tk.Button(announcement_control, text="Change view to Announcement",
                                         command=lambda: controller.show_frame("AnnounceDisplay"))
        but_announce_display.grid(row=1)

        ############################

    def quitApp(self):
        global app
        app.destroy()

    def changeOfficial(self, texti):
        global title_official

        if texti == "OFFICIAL":
            title_official['text'] = "OFFICIAL"
            print 1
        elif texti == "UNOFFICIAL":
            title_official['text'] = "UNOFFICIAL"
            print 2

    def changeOfficialText(self, entryy, default_entry):
        global title_official
        teks_ent = entryy.get()
        def_text = default_entry.get()
        if teks_ent == "":
            title_official['text'] = def_text
        else:
            title_official['text'] = teks_ent

    def update_display_image(self, path):
        global image_label
        temp = Image.open(path)
        temp = temp.resize((1366, 768))
        img2 = ImageTk.PhotoImage(temp)
        image_label.configure(image=img2)
        image_label.image = img2
        self.controller.show_frame("ImageDisplay")


class FinalAnnounceDisplay(tk.Frame):

    def __init__(self, parent, controller):
        # global queryyy,label_final
        tk.Frame.__init__(self, parent)
        self.controller = controller
        # label_final = tk.Label(self, text="Go to Finals", font=controller.title_font)
        # label_final.pack(side="top", fill="x", pady=10)


class MedalDisplay(tk.Frame):

    def __init__(self, parent, controller):
        global container_medal, container_medal_result
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.frame_title_medal = tk.Frame(self, width=def_w, height=90, bg="black")
        self.frame_title_medal.pack_propagate(0)
        self.frame_title_medal.pack()

        self.frame_official_medal = tk.Frame(self, width=def_w, height=40, bg="gray")
        self.frame_official_medal.pack_propagate(0)
        self.frame_official_medal.pack()

        container_medal = tk.Frame(self, bg="black", width=def_w, height=1236)
        container_medal.pack(anchor=tk.CENTER, fill=tk.BOTH, expand=True)
        container_medal.grid_rowconfigure(0, weight=1)
        container_medal.grid_columnconfigure(0, weight=1)

        self.frames = {}
        self.frames["MedalImage"] = MedalImage(parent=container_medal, controller=self)
        container_medal_result = MedalResult(parent=container_medal, controller=self)

        self.frames["MedalImage"].grid(row=0, column=0, sticky="nsew")
        container_medal_result.grid(row=0, column=0, sticky="nsew")

        self.show_medal_frame("MedalResult")

    def show_medal_frame(self, page_name):
        global container_medal_result
        if page_name == "MedalResult":
            frame = container_medal_result
        else:
            frame = self.frames[page_name]

        frame.tkraise()


class MedalImage(tk.Frame):
    def __init__(self, parent, controller):
        global medal_image_label
        tk.Frame.__init__(self, parent, bg="black")
        self.controller = controller
        # medal_image_label = tk.Label(self,image="",bg="black")
        # medal_image_label.pack(anchor=tk.CENTER,fill="x")
        # temp = Image.open("imgs/medal.png")
        # # temp = temp.resize((1366, 700))
        # img2 = ImageTk.PhotoImage(temp)
        # medal_image_label.configure(image=img2)
        # medal_image_label.image = img2


class MedalResult(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        # medal_image_label = tk.Label(self,image="",text="Medal Result")
        # medal_image_label.pack(side="top",fill="x")


class ImageDisplay(tk.Frame):

    def __init__(self, parent, controller):
        global image_label
        tk.Frame.__init__(self, parent)
        self.controller = controller
        image_label = tk.Label(self, text="Image", image="")
        image_label.pack(side="top", fill="x")


class AnnounceDisplay(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Announcement", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)


if __name__ == "__main__":
    app = App()
    app.mainloop()
