import pyodbc
import datetime
import sqlite3

conn_str = (
    r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
    r'DBQ=C:\display-manager-xml\DB\PIAG_2018_Database.mdb;'
    r'PWD=TeP69s)lAd_mW-(J_72u'
    )
mscnxn = pyodbc.connect(conn_str,readonly=True)
mscur = mscnxn.cursor()

mscur.execute('SELECT Event.Event_no,RecordsbyEvent.tag_ptr,RecordsbyEvent.Record_Time FROM RecordsbyEvent LEFT JOIN Event ON Event.Event_ptr = RecordsbyEvent.event_ptr WHERE RecordsbyEvent.tag_ptr=1 ORDER BY RecordsbyEvent.event_ptr')
rows = mscur.fetchall()

mscnxn.close()

db = sqlite3.connect('local.db')
cur = db.cursor()

for row in rows:
    time = datetime.timedelta(seconds=row[2])
    h,m,s = str(time).split(':')
    time = str(m)+':'+str(s[:5])
    query = 'UPDATE EventList SET even_nr="'+str(time)+'" WHERE even_number="'+str(row[0])+'"'
    cur.execute(query)
db.commit()
db.close()

#########################################

mscnxn = pyodbc.connect(conn_str,readonly=True)
mscur = mscnxn.cursor()

mscur.execute('SELECT Event.Event_no,RecordsbyEvent.tag_ptr,RecordsbyEvent.Record_Time FROM RecordsbyEvent LEFT JOIN Event ON Event.Event_ptr = RecordsbyEvent.event_ptr WHERE RecordsbyEvent.tag_ptr=2 ORDER BY RecordsbyEvent.event_ptr')
rows = mscur.fetchall()

mscnxn.close()

db = sqlite3.connect('local.db')
cur = db.cursor()

for row in rows:
    time = datetime.timedelta(seconds=row[2])
    h,m,s = str(time).split(':')
    time = str(m)+':'+str(s[:5])
    query = 'UPDATE EventList SET even_mr="'+str(time)+'" WHERE even_number="'+str(row[0])+'"'
    cur.execute(query)
db.commit()
db.close()