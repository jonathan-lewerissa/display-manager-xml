from Tkinter import *
from time import sleep
from threading import Thread
import socket, sys
from struct import *
import datetime
import sqlite3
from lxml import etree
from manager_xml import *
from update_xml import *
from multiprocessing import Process
import thread
from time import sleep
from PIL import Image, ImageTk

#Global Var
counter=0.0
minutes=0
time_string=""
n_iter=8
s=0
e=8
s1=0
e1=8
first=0.0
event_number=1
del_list=[]
del_list1=[]
queryyy=None
heatt=None
mode_lomba=None
pesertas=None
name_labels_frames=[]
teams=[]
heat_=None
laps=None
split_counter=0
total_competitor=0
competitor_split_count={}
prev_time_catch = {}
ranked_names=[]
ranked_times=[]
ranked_lanes=[]
ranked_teams=[]
pesertas_number=0
counter_pesertas=0
replaced_names=[]
replaced_times=[]
lane_labels=[]
nameeee={}
teamnamee={}
title_something=None
replaced_teams=[]
dns_dict={}
timer_now=None
batas_lane=8
h_comp=65
# batas_lane=10
# h_comp=50
#Global Var
puyo=1
def listener(home,label,status_label,pos_label,name_frames): 
	global first,nameeee,teamnamee,puyo,dns_dict
	global laps,timer_now
	global competitor_split_count,name_labels_frames,pesertas_number,counter_pesertas
	global ranked_names,ranked_times,ranked_lanes,ranked_teams
	#create an INET, STREAMing socket
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
		s.bind(('',10000))
	except socket.error as msg:
		print('Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
		sys.exit()
	 
	# receive a packet
	while True:
		try:
			packet = s.recvfrom(65565)
			 
			#packet string from tuple
			packet = packet[0]
				 
			#take first 20 characters for the ip header
			ip_header = packet[0:20]
				 
			#now unpack them :)
			iph = unpack('!BBHHHBBH4s4s' , ip_header)
				 
			version_ihl = iph[0]
			version = version_ihl >> 4
			ihl = version_ihl & 0xF
				 
			iph_length = ihl * 4
				 
			# ttl = iph[5]
			# protocol = iph[6]
			# s_addr = socket.inet_ntoa(iph[8])
			# d_addr = socket.inet_ntoa(iph[9])
				 
			# print 'Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(s_addr) + ' Destination Address : ' + str(d_addr)
				 
			tcp_header = packet[iph_length:iph_length+20]
				 
			#now unpack them :)
			tcph = unpack('!HHLLBBHHH' , tcp_header)
				 
			# source_port = tcph[0]
			# dest_port = tcph[1]
			# sequence = tcph[2]
			# acknowledgement = tcph[3]
			doff_reserved = tcph[4]
			tcph_length = doff_reserved >> 4
				 
			# print 'Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Sequence Number : ' + str(sequence) + ' Acknowledgement : ' + str(acknowledgement) + ' TCP header length : ' + str(tcph_length)
				 
			h_size = iph_length + tcph_length * 4
			data_size = len(packet) - h_size
			 
			#get data from the packet
			data = packet[h_size:].split()
		except:
			continue
		
		if "ReactionTime" in data:
			ind=data.index("ReactionTime")
			print(data[ind],data[ind+1])
			react_index=int(data[ind-1])-1
			timeee=float(data[ind+1])/float(10000)
			if timeee>=60.0:			
				te=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				te=minu+':'+seco[:5]
			else:
				te=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				te=seco[:5]
	
			print(str(te))
			print(str(react_index))
			update_reaction(react_index+1,te)
			srr="+"+str(te)
			lab3=Label(status_label[react_index],text=srr,bg="white",fg="black",font=("Veldana",40,"bold"))
			lab3.pack()
			del_list.append(lab3)
			lab3.after(6000,lambda:destroy_label(False))
			continue
		if "RunningTime" in data:
			ind=data.index("RunningTime")
			print(data[ind],data[ind+1])
			timeee=float(data[ind+1])/float(10000)
			if timeee<0.0 or timeee==-0.0:
				timeee=float(0.0)
			if timeee>=60.0:			
				t=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				t=minu+':'+seco[:5]
			else:
				t=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				t=seco[:5]
			
			if t==0.0:
				t==""
			label['text']=t
			try:
				update_timing(str(t))
			except:
				print('cannot update timing')
				continue
			continue
		if "LaneTime" in data:

			ind=data.index("LaneTime")
			lane_index=int(data[ind-1])-1

			print(data[ind],data[ind+1])

			if int(data[ind+1]) < 0:
				label['text']=0.0
				continue
			print 'lane index:'+str(lane_index)
			if lane_index<0:
				continue
			if prev_time_catch[lane_index] == data[ind+1]:
				continue
			prev_time_catch[lane_index] = data[ind+1]
			try:
				competitor_split_count[lane_index]+=1
			except:
				continue

			# print('split count:'+str(competitor_split_count[lane_index]))
			# print(competitor_split_count)
			# print(laps)
			# print(type(laps))
			# print(type(competitor_split_count[lane_index]))

			if lane_index<0 or lane_index>7:
				#lane_index=0
				continue
			rank=data[ind+3]
			print("rank : "+str(rank))
			print(type(rank))

			timeee=float(data[ind+1])/float(10000)
			print(timeee)
			if timeee>=60.0:
				if rank=='1':
					print('2')
					first=timeee
					splitting_time=str(datetime.timedelta(seconds=first))
					spl_h,spl_m,spl_s=splitting_time.split(":")
					splitting_time=str(spl_m)+":"+str(spl_s[:5])
					print first
				else:
					print('beda2')
					print first
					splitting_time=str(datetime.timedelta(seconds=timeee-first))
					spl_h,spl_m,spl_s=splitting_time.split(":")
					splitting_time="+"+str(spl_m)+":"+str(spl_s[:5])
				t=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				t=minu+':'+seco[:5]
			else:
				if rank=='1':
					print('1')
					first=timeee
					splitting_time=str(datetime.timedelta(seconds=first))
					print splitting_time
					spl_h,spl_m,spl_s=splitting_time.split(":")
					splitting_time=str(spl_m)+":"+str(spl_s[:5])
					print splitting_time
				else:
					print('beda')
					print first
					splitting_time=str(datetime.timedelta(seconds=timeee-first))
					spl_h,spl_m,spl_s=splitting_time.split(":")
					splitting_time="+"+str(spl_m)+":"+str(spl_s[:5])
					print splitting_time
				t=str(datetime.timedelta(seconds=timeee))
				hour,minu,seco=t.split(':')
				t=seco[:5]
			
			print(str(lane_index))
			update_split(str(lane_index+1),str(rank),str(t),str(splitting_time))
			lab=Label(status_label[lane_index],text=t,bg="white",fg="black",font=("Veldana",40,"bold"))
			lab1=Label(pos_label[lane_index],text=rank,bg="white",fg="black",font=("Veldana",40,"bold"))
			try:
				temp_cek=dns_dict[lane_index]
			except:
				temp_cek=0
			if lab!=temp_cek:
				lab.pack()
				lab1.pack()
				del_list.append(lab)
				del_list1.append(lab1)

			print(int(competitor_split_count[lane_index]),int(laps))
			
			if int(competitor_split_count[lane_index]) < int(laps):
				print("splithehe:"+str(competitor_split_count[lane_index]))
				home.after(13000,lambda:destroy_label(True))
			else:
				counter_pesertas+=1
				print("splithehe:"+str(competitor_split_count[lane_index]))
				print(competitor_split_count[lane_index])
				temp=nameeee[lane_index]
				temp_team=teamnamee[lane_index]
				ranked_names.append(temp['text'])
				ranked_times.append(str(t))
				ranked_lanes.append(lane_index)
				ranked_teams.append(temp_team['text'])

				update_current_result(str(lane_index+1),str(t),puyo)
				
				queryyy[0],heatt,queryyy[2]

				db = sqlite3.connect('local.db')
				cur = db.cursor()
				time_update_query = 'UPDATE StartList SET competitor_time="'+str(t)+'" WHERE competitor_lane='+str(lane_index+1)+' AND even_number='+str(queryyy[0])+' AND even_type="'+str(queryyy[2])+'" AND even_heat='+str(heatt)
				print time_update_query
				cur.execute(time_update_query)
				db.commit()
				db.close()

				puyo+=1
				print ranked_names
				print ranked_teams
				# home.after(60000,lambda:destroy_label(True))
				print counter_pesertas,pesertas_number
				if counter_pesertas==pesertas_number:
					print('masuk')
					changeOfficial()
					# for i in range(0,len(ranked_names)):
					# 	update_current_result(ranked_lanes[i],ranked_times[i],str(i+1))
					#home.after(6000,lambda:replace_ranked(ranked_names,name_frames,ranked_times,status_label))

def dns(status_frames,lane):
	global dns_dict
	try:
		temp=dns_dict[lane]
	except:
		print "go"
	if temp['text']=="DNS":
		lab=Label(status_frames[lane],text="",bg="white",fg="black",font=("Veldana",40,"bold"))
	else:
		lab=Label(status_frames[lane],text="DNS",bg="white",fg="black",font=("Veldana",40,"bold"))
	dns_dict[lane]=lab
	lab.pack()

def replace_ranked(ranked_names,name_frames,ranked_times,status_label,ranked_teams,team_frames,lane_frames):
	print('replace_ranked')
	global name_labels_frames,lane_labels,teams,del_list1,del_list
	global replaced_names,replaced_times,replaced_teams
	for element in teams:
		element.destroy()
	teams=[]
	for element in name_labels_frames:
		element.destroy()
	for element in del_list1:
		element.destroy()
	for element in del_list:
		element.destroy()
	del_list=[]
	del_list1=[]
	name_labels_frames=[]
	for i in range(0,len(ranked_names)):
		temp=Label(name_frames[i],text=ranked_names[i],fg="white",bg="pink4",font=("Veldana",37,"bold"))
		replaced_names.append(temp)
		replaced_times.append(Label(status_label[i],text=ranked_times[i],fg="black",bg="white",font=("Veldana",37,"bold")))
	for i in range(0,len(ranked_teams)):
		temp=Label(team_frames[i],text=ranked_teams[i],fg="white",bg="black",font=("Veldana",37,"bold"))
		replaced_teams.append(temp)
	start_ranked=0
	for element in replaced_names:
		element.pack()
		# Label(lane_frames[i],text=str(start_ranked+1),bg="white",fg="black",font=("Times",40,"bold")).pack()
		# start_ranked+=1
	for element in replaced_teams:
		element.pack()
	for element in replaced_times:
		element.pack()
	for element in lane_labels:
		element['text']=str(start_ranked+1)
		start_ranked+=1
	# for i in range (0,len(lane_labels)):
	# 	temp=lane_labels[i]
	# 	temp['text']=i+1
	replaced_names=[]
	replaced_times=[]
	replaced_teams=[]

def changeOfficial():
	global title_something
	temp=title_something['text']

	if temp == "":
		title_something['text']="UNOFFICIAL"
	elif temp=="UNOFFICIAL":
		title_something['text']="OFFICIAL"
		print 1
	elif temp=="OFFICIAL":
		title_something['text']="UNOFFICIAL"
		print 2

def disq(status_frames,ind):
	lab=Label(status_frames[ind],text="DQ",bg="white",fg="black",font=("Veldana",40,"bold"))
	lab.pack()

def destroy_label(flag):
	global s,e,del_list,del_list1
	if flag==True:
		del_list[0].destroy()
		del_list1[0].destroy()
		del del_list[0]
		del del_list1[0]
	elif flag==False:
		del_list[0].destroy()
		del del_list[0]

def text_toDisplay(time_res,r_index,lab,pos_frames,status_frames):
	global s1,e1,del_list,del_list1
	#for i in range(0,len(time_res)):
	if s1<e1:
		ind=r_index.index(s1+1)
		bel=Label(status_frames[ind],text=time_res[s1],bg="white",fg="black",font=("Veldana",40,"bold"))
		bel1=Label(pos_frames[ind],text=s1+1,bg="white",fg="black",font=("Veldana",40,"bold"))
		bel.pack()
		bel1.pack()
		del_list.append(bel)
		del_list1.append(bel1)
		s1+=1
		lab.after(500,lambda:text_toDisplay(time_res,r_index,lab,pos_frames,status_frames))
	
def time_disp(n):
	#print "Pada time_disp "+str(n)
	total=0
	tot_list=[]
	range_plus=[0.41,0.31,0.87,1.23,0.3,0.65,1.01]
	
	for i in range(0,8):
		if i==0:
			total+=n
		else:
			total+=range_plus[i-1]
		tot_list.append(round(total,2))
		
	random_index=ran_index()
	#print tot_list
	return tot_list,random_index
		
def Count(lab,pos_frames,status_frames):
	global counter,minutes,time_string,s,s1,del_list,del_list1
	#print "Counter : "+str(counter)
	if counter>23.43 and counter < 23.44 and time_string=="" or counter>50.21 and counter < 50.22 and time_string=="":
		#print counter
		time_res,r_index=time_disp(counter)
		s1=0
		text_toDisplay(time_res,r_index,lab,pos_frames,status_frames)
		s=0
		lab.after(6000,lambda:destroy_label(lab))
	if counter>=60:		
		#print "masuk if"
		minutes+=1
		counter=0.0
		x=str(minutes)+":"+str(counter)
		time_string=str(minutes)+":"
	else:
		#print "masuk else"
		x=str(counter)
	#print x
	lab['text']=time_string+str(x)
	counter+=0.01
	lab.after(10, lambda:Count(lab,pos_frames,status_frames))
	
def get_events():
	name_=[]
	db = sqlite3.connect('local.db')
	cur = db.cursor()
	query="SELECT * from EventList LIMIT 30"
	
	cur.execute(query)
	
	rows=cur.fetchall()
	
	for row in rows:
		row=list(row)
		name_.append(str(row[0])+":"+str(row[1])+":"+str(row[5]))
	
	db.close()
	return name_
	
def get_pesertas(id_event,type_event,heat,name_frames,team_frames,lane_frames):
	global pesertas,teams,heat_,nameeee
	global name_labels_frames
	global total_competitor
	global laps
	global competitor_split_count
	global pesertas_number
	global prev_time_catch
	global lane_labels
	name_=[]
	db = sqlite3.connect('local.db')
	cur = db.cursor()
	query_last="select * from StartList where even_number="+str(id_event)+" and even_type='"+str(type_event)+"' and even_heat="+str(int(heat)-1)
	query="select * from StartList where even_number="+str(id_event)+" and even_type='"+str(type_event)+"' and even_heat="+str(int(heat))
	query_next="select * from StartList where even_number="+str(id_event)+" and even_type='"+str(type_event)+"' and even_heat="+str(int(heat)+1)

	heat_n="select even_length from EventList where even_number="+str(id_event)
	print query

	cur.execute(query)
	rows=cur.fetchall()
		
	cur.execute(query_next)
	rows_next=cur.fetchall()

	cur.execute(heat_n)
	heat_=cur.fetchone()
	laps2=heat_[0]
	laps = laps2/50
	for row in rows:
		name_.append(list(row))

	total_competitor = len(rows)

	name_next_=[]
	for row in rows_next:
		name_next_.append(list(row))
	
	db.close()
	pesertas=name_
	# generate_current_result(pesertas)
	# generate_split(pesertas)
	generate_running_time()
	# generate_current_heat(pesertas)
	# generate_next_heat(name_next_)
	pesertas_number=len(pesertas)
	
	for element in name_labels_frames:
		element.destroy()
	for element in teams:
		element.destroy()
	name_labels_frames=[]
	teams=[]
	print len(name_labels_frames)
	for element in pesertas:
		ind=int(element[4])-1
		competitor_split_count[ind]=0
		name=str(element[5])+" "+str(element[6])
		name_labels_frames.append(Label(name_frames[ind],text=name,fg="white",bg="pink4",font=("Veldana",37,"bold")))
		nameeee[ind]=Label(name_frames[ind],text=name,fg="white",bg="pink4",font=("Veldana",37,"bold"))
		teams.append(Label(team_frames[ind],text=element[7],fg="white",bg="black",font=("Veldana",37,"bold")))
		teamnamee[ind]=Label(team_frames[ind],text=element[7],fg="white",bg="black",font=("Veldana",37,"bold"))

	lane_labels=[]
	for i in range(0,len(pesertas)):
		lane=Label(lane_frames[i],text="L"+str(i+1),bg="white",fg="black",font=("Veldana",40,"bold"))
		lane_labels.append(lane)
		prev_time_catch[i] = None
	for element in lane_labels:
		element.pack()

	print len(name_labels_frames)
	for i in range (0,len(name_labels_frames)):
		name_labels_frames[i].pack(side=LEFT)
	for element in teams:
		element.pack(side=LEFT)

def clear_window(frames):
	# print len(frames)
	# for element in frames:
	# 	print element
	# 	print len(element)
	for element in frames:
		print element
		if not element:
			continue
		else:
			for frame in element:
				temp_child=frame.winfo_children()
				try:
					for child in temp_child:
						child.destroy()
				except:
					print temp_child
					temp_child.destroy()
	
def getText(var,title,mode_label):
	global queryyy
	global mode_lomba
	queryyy=var.get()
	queryyy=queryyy.split(":")
	title['text']=queryyy[0]+" - "+queryyy[1]
	if queryyy[2]=="F":
		mode_lomba="Final"
	elif queryyy[2]=="P":
		mode_lomba="Preliminary"
	mode_label['text']=mode_lomba

def getHeat(var,mode_label,framess):
	global heatt, mode_lomba, counter_pesertas
	global ranked_names,ranked_teams,ranked_times
	global title_something
	ranked_names=[]
	ranked_teams=[]
	ranked_times=[]
	counter_pesertas=0
	title_something['text']=""
	clear_window(framess)
	heatt=var.get()
	mode_label['text']=str(mode_lomba)+" "+str(heatt)

def update_waktu():
	global timer_now
	print "Masuk Process"
	waktu=str(datetime.datetime.time(datetime.datetime.now()))
	try:
		waktu,sisa=waktu.split('.')
		timer_now['text']=waktu
	except:
		timer_now['text']=waktu
	return

def get_final():
	global queryyy
	db = sqlite3.connect('local.db')
	cur = db.cursor()

	# final_query = 'SELECT * FROM EventList WHERE even_type="P" AND even_number='+str(queryyy[0])
	# cur.execute(final_query)
	# row=list(cur.fetchone())
	# row[5]='F'
	# cur.execute('INSERT INTO EventList VALUES (?,?,?,?,?,?)',row)

	final_query = 'SELECT * FROM StartList WHERE even_number='+str(queryyy[0])+' ORDER BY competitor_time LIMIT 10'
	cur.execute(final_query)
	rows=cur.fetchall()
	print rows
	result_rows=[]
	for row in rows:
		row=list(row)
		result_rows.append(row)

	finalist_window=Toplevel()
	finalist_window.title("DisMan-Proto")
	finalist_window.geometry("1366x768")
	def_w=1366
	batas_lane=10
	h_comp=50
	competitor_frames=[]
	lane_frames=[]
	team_frames=[]
	logo_frames=[]
	name_frames=[]
	pos_frames=[]
	status_frames=[]
	###defines - Stop
	
	###Layout Design - Start
	#Frame Title - Start
	frame_title=Frame(finalist_window,width=def_w,height=90,bg="black")
	frame_title.pack_propagate(0)
	frame_title.pack()
	#Frame Title - Stop
	
	#Frame Bawah Title - Start
	frame_something=Frame(finalist_window,width=def_w,height=40,bg="gray")
	frame_something.pack_propagate(0)
	frame_something.pack()
	#Frame Bawah Title - Stop
	
	#Frame Competitor - Start
	for i in range(0,batas_lane):
		temp=Frame(finalist_window,width=def_w,height=h_comp,bg="gray",bd=1)
		competitor_frames.append(temp)
	for i in range(0,batas_lane):
		competitor_frames[i].pack()
		separator = Frame(height=2, bd=1, relief=SUNKEN)
		separator.pack()
	#Frame Competitor - Stop
		
	#Frame Timer - Start	
	frame_timer=Frame(finalist_window,width=def_w,height=102,bg="white")
	frame_timer.pack_propagate(0)
	frame_timer.pack()
	#Frame Timer - Stop
	###Layout Design - Stop
	
	
	###Competitor Content Frames - Start
	#Frame Lane - Start
	for i in range(0,batas_lane):
		temp=Frame(competitor_frames[i],width=100,height=h_comp,bg="white")
		lane_frames.append(temp)
	for i in range(0,batas_lane):
		lane_frames[i].pack_propagate(0)
		lane_frames[i].pack(side=LEFT)
		separator1 = Frame(width=2,height=65, bd=1, relief=SUNKEN)
		separator1.pack(side=LEFT)
	#Frame Lane - Stop
	
	#Frame Team - Start
	for i in range(0,batas_lane):	
		temp=Frame(competitor_frames[i],width=100,height=h_comp,bg="black")
		temp1=Frame(competitor_frames[i],width=100,height=h_comp,bg="black")
		team_frames.append(temp)
		logo_frames.append(temp1)
	for i in range(0,batas_lane):
		team_frames[i].pack_propagate(0)
		team_frames[i].pack(side=LEFT)
		logo_frames[i].pack_propagate(0)
		logo_frames[i].pack(side=LEFT)
	#Frame Team - Stop

	#Frame CompName - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=600,height=h_comp,bg="pink4")
		name_frames.append(temp1)
	for i in range(0,batas_lane):
		name_frames[i].pack_propagate(0)
		name_frames[i].pack(side=LEFT)
	#Frame CompName - Stop
	
	#Frame Pos - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=100,height=h_comp,bg="white")
		pos_frames.append(temp1)
	for i in range(0,batas_lane):
		pos_frames[i].pack_propagate(0)
		pos_frames[i].pack(side=LEFT)
		separator2 = Frame(competitor_frames[i],width=2,height=h_comp, bd=1, relief=SUNKEN,bg="black")
		separator2.pack(side=LEFT)
	#Frame Pos - Stop
	
	#Frame Status - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=364,height=h_comp,bg="white")
		status_frames.append(temp1)	
	for i in range(0,batas_lane):
		status_frames[i].pack_propagate(0)
		status_frames[i].pack(side=LEFT)
	#Frame Status - Stop
	###Competitor Content Frames - Stop
	
	###Title Content
	image = Image.open("imgs/logosss.png")
	photo = ImageTk.PhotoImage(image)
	logo_label=Label(frame_title,image=photo)
	logo_label.image=photo
	logo_label.pack(side="left")
	title_label=Label(frame_title,text=queryyy[0]+" - "+queryyy[1],bg="black",fg="white",font=("Veldana",30,"bold"))
	title_label.pack()
	mode_label=Label(frame_title,text="Final Result",bg="black",fg="white",font=("Veldana",30,"bold"))
	mode_label.pack()
	###Title Content
	
	title_something=Label(frame_something,text="",bg="gray",fg="black",font=("Veldana",15,"bold"))
	title_something.pack()
	###Timer - Start
	sep=Frame(frame_timer,width=20,height=65, relief=SUNKEN,bg="white")
	sep.pack(side=RIGHT)
	waktu_skrg=str(datetime.datetime.time(datetime.datetime.now()))
	try:
		waktu_skrg,sisa=waktu_skrg.split('.')
		print waktu_skrg
	except:
		print "masuk except"
		print waktu_skrg
	timer_now=Label(frame_timer,text=waktu_skrg,fg="black",bg="white",font=("Veldana",35,"bold"))
	timer_now.pack(side=LEFT)

	timer_label=Label(frame_timer,text=0.0,fg="black",bg="white",font=("Veldana",40,"bold"))
	timer_label.pack(side=RIGHT)

	for i in range(0,len(team_frames)):
		temp=result_rows[i]
		Label(team_frames[i],text=temp[7],fg="white",bg="black",font=("Veldana",37,"bold")).pack()
		Label(name_frames[i],text=temp[5]+" "+temp[6],fg="white",bg="pink4",font=("Veldana",37,"bold")).pack()
		if i>7:
			Label(pos_frames[i],text="R",bg="white",fg="black",font=("Veldana",40,"bold")).pack()
		Label(pos_frames[i],text="Q",bg="white",fg="black",font=("Veldana",40,"bold")).pack()
		Label(lane_frames[i],text=str(i+1),bg="white",fg="black",font=("Veldana",40,"bold")).pack()


def main():
	global queryyy,heatt,pesertas,lane_labels,ranked_names,ranked_times,title_something,ranked_teams
	global timer_now
	global batas_lane,h_comp
	home=Tk()
	home.title("DisMan-Proto")
	home.geometry("1366x768")
	###defines - Start
	def_w=1366
	competitor_frames=[]
	lane_frames=[]
	team_frames=[]
	logo_frames=[]
	name_frames=[]
	pos_frames=[]
	status_frames=[]
	###defines - Stop
	
	###Layout Design - Start
	#Frame Title - Start
	frame_title=Frame(width=def_w,height=90,bg="black")
	frame_title.pack_propagate(0)
	frame_title.pack()
	#Frame Title - Stop
	
	#Frame Bawah Title - Start
	frame_something=Frame(width=def_w,height=40,bg="gray")
	frame_something.pack_propagate(0)
	frame_something.pack()
	#Frame Bawah Title - Stop
	
	#Frame Competitor - Start
	for i in range(0,batas_lane):
		temp=Frame(width=def_w,height=h_comp,bg="gray",bd=1)
		competitor_frames.append(temp)
	for i in range(0,batas_lane):
		competitor_frames[i].pack()
		separator = Frame(height=2, bd=1, relief=SUNKEN)
		separator.pack()
	#Frame Competitor - Stop
		
	#Frame Timer - Start	
	frame_timer=Frame(width=def_w,height=102,bg="white")
	frame_timer.pack_propagate(0)
	frame_timer.pack()
	#Frame Timer - Stop
	###Layout Design - Stop
	
	
	###Competitor Content Frames - Start
	#Frame Lane - Start
	for i in range(0,batas_lane):
		temp=Frame(competitor_frames[i],width=100,height=h_comp,bg="white")
		lane_frames.append(temp)
	for i in range(0,batas_lane):
		lane_frames[i].pack_propagate(0)
		lane_frames[i].pack(side=LEFT)
		separator1 = Frame(width=2,height=65, bd=1, relief=SUNKEN)
		separator1.pack(side=LEFT)
	#Frame Lane - Stop
	
	#Frame Team - Start
	for i in range(0,batas_lane):	
		temp=Frame(competitor_frames[i],width=100,height=h_comp,bg="black")
		temp1=Frame(competitor_frames[i],width=100,height=h_comp,bg="black")
		team_frames.append(temp)
		logo_frames.append(temp1)
	for i in range(0,batas_lane):
		team_frames[i].pack_propagate(0)
		team_frames[i].pack(side=LEFT)
		logo_frames[i].pack_propagate(0)
		logo_frames[i].pack(side=LEFT)
	#Frame Team - Stop
	
	#Frame CompName - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=600,height=h_comp,bg="pink4")
		name_frames.append(temp1)
	for i in range(0,batas_lane):
		name_frames[i].pack_propagate(0)
		name_frames[i].pack(side=LEFT)
	#Frame CompName - Stop
	
	#Frame Pos - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=100,height=h_comp,bg="white")
		pos_frames.append(temp1)
	for i in range(0,batas_lane):
		pos_frames[i].pack_propagate(0)
		pos_frames[i].pack(side=LEFT)
		separator2 = Frame(competitor_frames[i],width=2,height=h_comp, bd=1, relief=SUNKEN,bg="black")
		separator2.pack(side=LEFT)
	#Frame Pos - Stop
	
	#Frame Status - Start
	for i in range(0,batas_lane):
		temp1=Frame(competitor_frames[i],width=364,height=h_comp,bg="white")
		status_frames.append(temp1)	
	for i in range(0,batas_lane):
		status_frames[i].pack_propagate(0)
		status_frames[i].pack(side=LEFT)
	#Frame Status - Stop
	###Competitor Content Frames - Stop
	
	###Title Content
	image = Image.open("imgs/logosss.png")
	photo = ImageTk.PhotoImage(image)
	logo_label=Label(frame_title,image=photo)
	logo_label.image=photo
	logo_label.pack(side="left")
	title_label=Label(frame_title,text="<Event>",bg="black",fg="white",font=("Veldana",30,"bold"))
	title_label.pack()
	mode_label=Label(frame_title,text="",bg="black",fg="white",font=("Veldana",30,"bold"))
	mode_label.pack()
	###Title Content
	
	title_something=Label(frame_something,text="",bg="gray",fg="black",font=("Veldana",15,"bold"))
	title_something.pack()
	###Timer - Start
	sep=Frame(frame_timer,width=20,height=65, relief=SUNKEN,bg="white")
	sep.pack(side=RIGHT)
	waktu_skrg=str(datetime.datetime.time(datetime.datetime.now()))
	try:
		waktu_skrg,sisa=waktu_skrg.split('.')
		print waktu_skrg
	except:
		print "masuk except"
		print waktu_skrg
	timer_now=Label(frame_timer,text=waktu_skrg,fg="black",bg="white",font=("Veldana",35,"bold"))
	timer_now.pack(side=LEFT)

	timer_label=Label(frame_timer,text=0.0,fg="black",bg="white",font=("Veldana",40,"bold"))
	timer_label.pack(side=RIGHT)
	###Timer - Stop
	
	###Lane Numbers - Start
	# for i in range(0,batas_lane):
	# 	lane=Label(lane_frames[i],text="L"+str(i+1),bg="white",fg="black",font=("Veldana",40,"bold"))
	# 	lane_labels.append(lane)
	# 	prev_time_catch[i] = None
	# for element in lane_labels:
	# 	element.pack()
	###Lane Numbers - Stop
	#home.after_idle(lambda:Count(timer_label,pos_frames,status_frames))
	th=Thread(target=lambda:listener(home,timer_label,status_frames,pos_frames,name_frames))
	th.start()
	###ControlPanel
	controlPanel=Toplevel()
	controlPanel.title("Control Panel")
	
	var = StringVar(controlPanel)
	var.set("Choose One") # initial value
	options=get_events()
	option=apply(OptionMenu,(controlPanel,var)+tuple(options))
	option.grid(row=0,column=0)

	buttonEvent = Button(controlPanel, text="Choose Events",command=lambda:getText(var,title_label,mode_label))
	buttonEvent.grid(row=0,column=1)
	
	
	var1 = StringVar(controlPanel)
	var1.set("Choose One") # initial value
	options1=[1,2,3,4,5]
	option1=apply(OptionMenu,(controlPanel,var1)+tuple(options1))
	option1.grid(row=1,column=0)
	
	
	butHeat=Button(controlPanel,text="Choose Heat",command=lambda:getHeat(var1,mode_label,[lane_frames,team_frames,logo_frames,name_frames,pos_frames,status_frames]))
	butHeat.grid(row=1,column=1)
	butPes=Button(controlPanel,text="Show",command=lambda:get_pesertas(queryyy[0],queryyy[2],heatt,name_frames,team_frames,lane_frames))
	butPes.grid(row=2)
	
	#buttons
	teks=[]
	for i in range(0,batas_lane):
		teks.append(i)
	butDQ=Button(controlPanel,text="L"+str(teks[0]+1)+"-DQ",command=lambda:disq(status_frames,teks[0]))
	butDQ.grid(row=3,column=3)

	butDQ1=Button(controlPanel,text="L"+str(teks[1]+1)+"-DQ",command=lambda:disq(status_frames,teks[1]))
	butDQ1.grid(row=3,column=4)

	butDQ2=Button(controlPanel,text="L"+str(teks[2]+1)+"-DQ",command=lambda:disq(status_frames,teks[2]))
	butDQ2.grid(row=3,column=5)

	butDQ3=Button(controlPanel,text="L"+str(teks[3]+1)+"-DQ",command=lambda:disq(status_frames,teks[3]))
	butDQ3.grid(row=3,column=6)

	butDQ4=Button(controlPanel,text="L"+str(teks[4]+1)+"-DQ",command=lambda:disq(status_frames,teks[4]))
	butDQ4.grid(row=3,column=7)

	butDQ5=Button(controlPanel,text="L"+str(teks[5]+1)+"-DQ",command=lambda:disq(status_frames,teks[5]))
	butDQ5.grid(row=3,column=8)

	butDQ6=Button(controlPanel,text="L"+str(teks[6]+1)+"-DQ",command=lambda:disq(status_frames,teks[6]))
	butDQ6.grid(row=3,column=9)

	butDQ7=Button(controlPanel,text="L"+str(teks[7]+1)+"-DQ",command=lambda:disq(status_frames,teks[7]))
	butDQ7.grid(row=3,column=10)
	#buttons

	butDNS=Button(controlPanel,text="L"+str(teks[0]+1)+"-DNS",command=lambda:dns(status_frames,teks[0]))
	butDNS.grid(row=4,column=3)

	butDNS1=Button(controlPanel,text="L"+str(teks[1]+1)+"-DNS",command=lambda:dns(status_frames,teks[1]))
	butDNS1.grid(row=4,column=4)

	butDNS2=Button(controlPanel,text="L"+str(teks[2]+1)+"-DNS",command=lambda:dns(status_frames,teks[2]))
	butDNS2.grid(row=4,column=5)

	butDNS3=Button(controlPanel,text="L"+str(teks[3]+1)+"-DNS",command=lambda:dns(status_frames,teks[3]))
	butDNS3.grid(row=4,column=6)

	butDNS4=Button(controlPanel,text="L"+str(teks[4]+1)+"-DNS",command=lambda:dns(status_frames,teks[4]))
	butDNS4.grid(row=4,column=7)

	butDNS5=Button(controlPanel,text="L"+str(teks[5]+1)+"-DNS",command=lambda:dns(status_frames,teks[5]))
	butDNS5.grid(row=4,column=8)

	butDNS6=Button(controlPanel,text="L"+str(teks[6]+1)+"-DNS",command=lambda:dns(status_frames,teks[6]))
	butDNS6.grid(row=4,column=9)

	butDNS7=Button(controlPanel,text="L"+str(teks[7]+1)+"-DNS",command=lambda:dns(status_frames,teks[7]))
	butDNS7.grid(row=4,column=10)

	butResultAkhir=Button(controlPanel,text="Get Result",command=lambda:replace_ranked(ranked_names,name_frames,ranked_times,status_frames,ranked_teams,team_frames,lane_frames))
	butResultAkhir.grid(row=4)

	butOfficiel=Button(controlPanel,text="Official",command=changeOfficial)
	butOfficiel.grid(row=5)

	butFinal=Button(controlPanel,text="Get Final for this Event",command=get_final)
	butFinal.grid(row=6)

	# while True:
	# 	update_waktu()
	# 	home.update_idletasks()
	# 	home.update()
	home.mainloop()

if __name__=="__main__":
	main()