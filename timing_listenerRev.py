import socket, sys
from struct import *
import datetime
from lxml import etree

def generate_timing(data):

	doc = etree.parse('C:/share_xml/Timing.xml')
	root = doc.getroot()

	code = root.xpath('//TIMING')

	if code:
		code[0].text = str(data)
		etree.ElementTree(root).write('C:/share_xml/Timing.xml',pretty_print=True)

def generate_lane_split(lane_index,rank,time,split_time):

	doc = etree.parse('xml/CurrentResult.xml')
	root = doc.getroot()

	rank_query = '//HEAT/COMPETITOR[COMPETITOR_LANE='+lane_index+']/COMPETITOR_RANK'
	result_query = '//HEAT/COMPETITOR[COMPETITOR_LANE='+lane_index+']/COMPETITOR_RESULT'
	split_query = '//HEAT/COMPETITOR[COMPETITOR_LANE='+lane_index+']/COMPETITOR_SPLIT'

	competitor_rank = root.xpath(rank_query)
	competitor_result = root.xpath(result_query)
	split_result = root.xpath(split_query)

	if competitor_rank:
		competitor_rank[0].text = str(rank)
		competitor_result[0].text = str(time)
		split_result[0].text = str(split_time)

		etree.ElementTree(root).write('xml/CurrentResult.xml',pretty_print=True)

first=0
#create an INET, STREAMing socket
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
	s.bind(('',1024))
except socket.error as msg:
	print('Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
	sys.exit()
 
# receive a packet
while True:
	packet = s.recvfrom(65565)
	#packet string from tuple
	packet = packet[0]
		 
	#take first 20 characters for the ip header
	ip_header = packet[0:20]
		 
	#now unpack them :)
	iph = unpack('!BBHHHBBH4s4s' , ip_header)
		 
	version_ihl = iph[0]
	version = version_ihl >> 4
	ihl = version_ihl & 0xF
		 
	iph_length = ihl * 4
		 
	# ttl = iph[5]
	# protocol = iph[6]
	# s_addr = socket.inet_ntoa(iph[8])
	# d_addr = socket.inet_ntoa(iph[9])
		 
	# print 'Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(s_addr) + ' Destination Address : ' + str(d_addr)
		 
	tcp_header = packet[iph_length:iph_length+20]
		 
	#now unpack them :)
	tcph = unpack('!HHLLBBHHH' , tcp_header)
		 
	# source_port = tcph[0]
	# dest_port = tcph[1]
	# sequence = tcph[2]
	# acknowledgement = tcph[3]
	doff_reserved = tcph[4]
	tcph_length = doff_reserved >> 4
		 
	# print 'Source Port : ' + str(source_port) + ' Dest Port : ' + str(dest_port) + ' Sequence Number : ' + str(sequence) + ' Acknowledgement : ' + str(acknowledgement) + ' TCP header length : ' + str(tcph_length)
		 
	h_size = iph_length + tcph_length * 4
	data_size = len(packet) - h_size
	 
	#get data from the packet
	data = packet[h_size:].split()
		
	if "ReactionTime" in data:
		continue
		ind=data.index("ReactionTime") 
		react_index=int(data[ind-1])-1
		timeee=float(data[ind+1])
		timeee=round(timeee/10000,2)
		if timeee>=60.0:			
			t=str(datetime.timedelta(seconds=timeee))
			hour,minu,seco=t.split(':')
			t=minu+':'+seco
		else:
			t=timeee
		try:
			t,y=t.split('.')
			y=y[:2]
			t=t+'.'+y
			print(str(lane_index))
			#lab=Label(status_label[react_index],text='+'+t,bg="white",fg="black",font=("Times",40,"bold"))
		except:
			print(str(lane_index))
			#lab=Label(status_label[react_index],text='+'+str(t),bg="white",fg="black",font=("Times",40,"bold"))
		#lab.pack()
		#del_list.append(lab)
		#lab.after(6000,lambda:destroy_label(False))

	if "RunningTime" in data:
		ind=data.index("RunningTime")
		print(data[ind],data[ind+1])
		timeee=float(data[ind+1])
		if timeee<0.0 or timeee==-0.0:
			timeee=float(0.0)
		timeee=round(timeee/10000,2)
		if timeee>=60.0:			
			t=str(datetime.timedelta(seconds=timeee))
			hour,minu,seco=t.split(':')
			t=minu+':'+seco
		else:
			t=timeee
		try:
			t,y=t.split('.')
			y=y[:2]
			t=t+'.'+y
			#label['text']=t
			try:
				print(str(t))
				# generate_timing(str(t))
			except:
				print('error')
				continue
		except:
			if t==0.0:
				t==""
			#label['text']=t
			try:
				print(str(t))
				# generate_timing(str(t))
			except:
				print('cannot write')
				continue
	if "LaneTime" in data:
		continue
		ind=data.index("LaneTime")
		lane_index=int(data[ind-1])-1
		if lane_index<0 or lane_index>7:
			#lane_index=0
			continue
		rank=data[ind+3]
		print("rank : "+str(rank))
		#timeee=float(data[ind+1])
		timeee=float(data[ind+1])
		timeee=round(timeee/10000,2)
		if timeee>=60.0:
			if rank==1:
				first=timeee
				splitting_time=str(datetime.timedelta(seconds=first))
			else:
				splitting_time=str("+"+datetime.timedelta(timeee-first))
			t=str(datetime.timedelta(seconds=timeee))
			hour,minu,seco=t.split(':')
			t=minu+':'+seco
		else:
			if rank==1:
				first=timeee
				splitting_time=str(datetime.timedelta(seconds=first))
			else:
				splitting_time=str("+"+datetime.timedelta(timeee-first))
			t=timeee
		try:
			t,y=t.split('.')
			y=y[:2]
			t=t+'.'+y
			print(str(lane_index))
			# generate_lane_split(str(lane_index+1),str(rank),str(t),str(splitting_time))
			#lab=Label(status_label[lane_index],text=t,bg="white",fg="black",font=("Times",40,"bold"))
		except:
			print(str(lane_index))
			# generate_lane_split(str(lane_index+1),str(rank),str(t),str())
			#lab=Label(status_label[lane_index],text=t,bg="white",fg="black",font=("Times",40,"bold"))
		#lab1=Label(pos_label[lane_index],text=rank,bg="white",fg="black",font=("Times",40,"bold"))
		#lab.pack()
		#lab1.pack()
		#del_list.append(lab)
		#del_list1.append(lab1)
		#lab.after(6000,lambda:destroy_label(True))
