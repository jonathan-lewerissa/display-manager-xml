from yattag import Doc, indent
import sqlite3
from lxml import etree

directory = 'xml/'

def update_timing(data):
	global directory

	doc = etree.parse(directory+'Timing.xml')
	root = doc.getroot()

	code = root.xpath('//TIMING')

	if code:
		code[0].text = str(data)
		etree.ElementTree(root).write(directory+'Timing.xml',pretty_print=True)

def update_reaction(lane,time):
	global directory

	doc = etree.parse(directory+'Reaction.xml')
	root = doc.getroot()

	code = root.xpath('//HEAT/REACTION[LANE='+str(lane)+']/TIME')

	if code:
		code[0].text = str(time)
		etree.ElementTree(root).write(directory+'Reaction.xml',pretty_print=True)

def update_split(lane_index,rank,time,split_time):
	global directory

	name_doc = etree.parse(directory+'CurrentHeat.xml')
	name_root = name_doc.getroot()

	name_res = name_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+lane_index+']/COMPETITOR_NAME')
	nation_res = name_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+lane_index+']/COMPETITOR_NATION')
	name = name_res[0].text
	nation = nation_res[0].text

	doc = etree.parse(directory+'Split.xml')
	root = doc.getroot()

	rank_query = '//HEAT/COMPETITOR[COMPETITOR_RANK='+rank+']/COMPETITOR_RANK'
	result_query = '//HEAT/COMPETITOR[COMPETITOR_RANK='+rank+']/COMPETITOR_SPLIT_TIME'
	split_query = '//HEAT/COMPETITOR[COMPETITOR_RANK='+rank+']/COMPETITOR_SPLIT_PLUS'
	name_query = '//HEAT/COMPETITOR[COMPETITOR_RANK='+rank+']/COMPETITOR_NAME'

	competitor_rank = root.xpath(rank_query)
	competitor_result = root.xpath(result_query)
	split_result = root.xpath(split_query)
	name_result = root.xpath(name_query)

	if competitor_rank:
		competitor_rank[0].text = str(rank)
		competitor_result[0].text = str(time)
		split_result[0].text = str(split_time)
		name_result[0].text = name

		etree.ElementTree(root).write(directory+'Split.xml',pretty_print=True)

def update_current_result(lane_index,result_time,rank):
	global directory
	doc = etree.parse(directory+'CurrentResult.xml')
	root = doc.getroot()

	id_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_ID')
	lane_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_LANE')
	name_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_NAME')
	firstname_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_FIRSTNAME')
	lastname_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_LASTNAME')
	teamname_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_TEAMNAME')
	nation_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_NATION')
	result_time_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_RESULT')
	flag_to_update = root.xpath('//HEAT/COMPETITOR[COMPETITOR_RANK='+str(rank)+']/COMPETITOR_FLAG')

	heat_doc = etree.parse(directory+'CurrentHeat.xml')
	heat_root = heat_doc.getroot()

	id_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_ID')
	lane_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_LANE')
	name_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_NAME')
	firstname_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_FIRSTNAME')
	lastname_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_LASTNAME')
	teamname_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_TEAMNAME')
	nation_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_NATION')
	result_time_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_RESULT')
	flag_from_update = heat_root.xpath('//HEAT/COMPETITOR[COMPETITOR_LANE='+str(lane_index)+']/COMPETITOR_FLAG')

	if result_time_to_update:
		id_to_update[0].text = str(id_from_update[0].text)
		lane_to_update[0].text = str(lane_index)
		name_to_update[0].text = str(name_from_update[0].text)
		firstname_to_update[0].text = str(firstname_from_update[0].text)
		lastname_to_update[0].text = str(lastname_from_update[0].text)
		teamname_to_update[0].text = str(teamname_from_update[0].text)
		nation_to_update[0].text = str(nation_from_update[0].text)
		result_time_to_update[0].text = str(result_time)
		flag_to_update[0].text = str(flag_from_update[0].text)

		etree.ElementTree(root).write(directory+'CurrentResult.xml',pretty_print=True)


